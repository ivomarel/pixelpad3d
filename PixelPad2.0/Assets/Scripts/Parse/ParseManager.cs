﻿using UnityEngine;
using System.Collections;
using System;

using System.Collections.Generic;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;
using GameSparks.Core;

public class ParseManager : Singleton<ParseManager>
{
    private const string TABLE_NAME = "Projects";
	private Action saveLevelSuccess;

    public bool isAuthorized;

    private IEnumerator Start()
    {
        
        yield return new WaitUntil(() => GS.Available);
        yield return null;
        new DeviceAuthenticationRequest().Send((response) =>
        {
            if (response.HasErrors) {
                Debug.Log(response.Errors.JSON);
            } else {
                Debug.Log("Authentication success!");
                isAuthorized = true;
            }
        });
    }

    public void SaveLevel (string levelName, string dataString, Action successCallback)
	{
		saveLevelSuccess = successCallback;
        new LogEventRequest_SaveProject().Set_Name(levelName).Set_Data(dataString).Send(OnSaveLevelResponse, OnSaveLevelFailed);
        
        //StartCoroutine (SaveLevelCor (levelName, dataString));
    }

    void OnSaveLevelResponse (LogEventResponse response)
    {
        if (saveLevelSuccess != null)
            saveLevelSuccess();
    }

    void OnSaveLevelFailed (LogEventResponse response)
    {
        Debug.Log("Failed");
    }

    /*
    private IEnumerator SaveLevelCor (string levelName, string dataString)
	{
        

        var query = ParseObject.GetQuery (TABLE_NAME).WhereEqualTo ("name", levelName);
		//When we call this query, we know to expect a ParseObject as a response
		Task<ParseObject> getLevelTask = query.FirstAsync ();

		while (!getLevelTask.IsCompleted)
			yield return null;

		if (getLevelTask.IsFaulted || getLevelTask.IsCanceled) {
			Debug.Log (getLevelTask.Exception.ToString ());
		} else {
			Debug.Log ("Found level, deleting...");
			//The result of our task is a ParseObject (which contains the level that we created before)
			ParseObject myLevel = getLevelTask.Result;
			Task deleteLevelTask = myLevel.DeleteAsync ();
			while (!deleteLevelTask.IsCompleted) {
				yield return null;
			}
		}

		ParseObject levelObject = new ParseObject (TABLE_NAME);
		levelObject ["name"] = levelName;
		levelObject ["data"] = dataString;

		Task t = levelObject.SaveAsync ();

		//We check every frame, if the Task has already completed. We can't use callbacks, since the Task is executed on a different Thread
		while (!t.IsCompleted) {
			yield return null;
		}

		if (t.IsFaulted || t.IsCanceled) {
			Debug.Log (t.Exception.ToString ());
		} else {
			//We always want to check if the callback is not null (otherwise it may throw an error)
			if (saveLevelSuccess != null)
				saveLevelSuccess ();
		}
	}
    */

	private Action<string> onFetchLevelsSuccess;

	public void FetchLevels (Action<string> callback)
	{
		onFetchLevelsSuccess = callback;
        //StartCoroutine (FetchLevelsCor ());
        new LogEventRequest_LoadProjects().Send(OnLoadProjectsResponse, OnLoadProjectsFailed);
	}

    void OnLoadProjectsResponse(LogEventResponse response)
    {
        GameSparks.Core.GSData gSData = response.ScriptData;
        onFetchLevelsSuccess(gSData.JSON);
    }

    void OnLoadProjectsFailed(LogEventResponse response)
    {
        Debug.Log(response.Errors.JSON);
    }

    private void OnGUI()
    {
        GUIStyle style = new GUIStyle();
        style.normal.textColor = Color.black;

        if (!GS.Available) {
            GUILayout.Label("Connecting...",style);
            return;
        }

        if (!isAuthorized) {
            GUILayout.Label("Authorizing...",style);
            return;
        }

        GUILayout.Label("Connected",style);
           
    }
}


























