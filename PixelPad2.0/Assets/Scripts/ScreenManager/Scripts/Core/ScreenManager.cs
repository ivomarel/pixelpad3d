﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine.UI;

public class ScreenManager : MonoBehaviour
{
	public static ScreenManager instance;
	
	public event Action<UIScreen> onPush;

	public UIScreen openingScreen;
	public bool pushOpeningScreen = true;
	public bool isLogging;

	internal Camera guiCamera;

	private const string SCREENS_PATH = "Screens";

	private Dictionary<Type, UIScreen> screens;
	
	private Stack<UIScreen> screenStack;

	private void Awake ()
	{
		instance = this;
		guiCamera = this.GetComponentInChildren<Camera> ();
		screens = new Dictionary<Type, UIScreen> ();
		screenStack = new Stack<UIScreen> ();

		UIScreen[] resourcesScreens = Resources.LoadAll<UIScreen> (SCREENS_PATH);

		foreach (UIScreen resourcesScreen in resourcesScreens) {
			resourcesScreen.gameObject.SetActive (false);
			UIScreen screen = Instantiate (resourcesScreen) as UIScreen;
			screen.GetComponent<RectTransform> ().SetParent (this.transform, false);
			screens.Add (screen.GetType (), screen);
            resourcesScreen.gameObject.SetActive(true);
		}
	}

	private void Start ()
	{
		if (pushOpeningScreen && openingScreen != null) {
			Push (openingScreen.GetType ());
		}
	}

	public T Push<T> () where T : UIScreen
	{
		return Push (typeof(T)) as T;
	}

	public UIScreen Push (Type screenType)
	{
		WriteLog ("Pushing " + screenType);
		if (!screens.ContainsKey (screenType)) {
			Debug.LogError (string.Format ("ScreenManager does not contain ({0})", screenType));
			return null;
		}
		UIScreen screen = screens [screenType];
		
		if (screenStack.Count != 0) {
			UIScreen topScreen = screenStack.Peek ();
			topScreen.OnLeave (screen);
		}
		
		screenStack.Push (screen);
		screen.transform.SetAsLastSibling ();
		screen.OnPush ();
		if (onPush != null)
			onPush (screen);
		return screen;
	}

	public bool Pop<T> () where T : UIScreen
	{
		WriteLog ("Try popping " + typeof(T));
		UIScreen topScreen = GetTopScreen ();
		if (topScreen != null && topScreen.GetType () == typeof(T) || topScreen.GetType ().BaseType == typeof(T)) {
			Pop ();
			return true;
		}
		return false;
	}
	
	public void Pop (bool instant = false)
	{
		UIScreen screen = screenStack.Pop ();
		WriteLog ("Popping " + screen.name);
		screen.OnPop (instant);
		UIScreen topScreen = GetTopScreen ();
		if (topScreen != null)
			topScreen.OnReturn (screen);
	}

	public void PopAllAbove<T> (bool includeLast = false) where T : UIScreen
	{
		if (!screenStack.Contains (GetScreenInstance<T> ()))
			return;

		UIScreen topScreen = GetTopScreen ();
		while (topScreen != null && topScreen.GetType() != typeof(T)) {
			Pop ();
			topScreen = GetTopScreen ();
		}
		if (includeLast) {
			Pop ();
		}
	}

	public void PopAllScreens ()
	{
		while (screenStack.Count != 0) {
			Pop ();
		}
	}

	public T GetScreenInstance<T> () where T : UIScreen
	{
		return screens [typeof(T)] as T;
	}


	public UIScreen GetTopScreen ()
	{
		if (screenStack.Count == 0) {
			return null;
		}
		return screenStack.Peek ();
	}

	private void WriteLog (object obj)
	{
		if (isLogging) {
			Debug.Log (string.Format ("ScreenManager: {0}", obj.ToString ()));
		}
	}

}
