﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UIScreen : MonoBehaviour
{
	public bool hideCurrent = true;

	protected UITween[] uiTweens {
		get {
			if (_uiTweens == null) {
				List<UITween> tweenList = new List<UITween> ();
				foreach (UITween tween in GetComponentsInChildren<UITween>(true)) {
					if (tween.playOnTransition) {
						tweenList.Add (tween);
					}
				}
				_uiTweens = tweenList.ToArray ();
				
			}
			return _uiTweens;
		}
	}

	private UITween[] _uiTweens;

	public virtual void OnPush ()
	{
		this.gameObject.SetActive (true);
		foreach (UITween uiTween in uiTweens) {
			uiTween.PlayForward ();
		}
	}

	public virtual void OnPop (bool instant = false)
	{
		if (instant || uiTweens.Length == 0) {
			Disable ();
		} else {
			foreach (UITween uiTween in uiTweens) {
				uiTween.PlayReverse (Disable);
			}
		}
	}

	public virtual void OnLeave (UIScreen toScreen)
	{
		if (toScreen.hideCurrent) {
			if (uiTweens.Length == 0) {
				Disable ();
			} else {
				foreach (UITween uiTween in uiTweens) {
					uiTween.PlayReverse (Disable);
				}
			}
		} 
	}

	public virtual void OnReturn (UIScreen fromScreen)
	{
		if (!this.gameObject.activeSelf) {
			this.gameObject.SetActive (true);
			foreach (UITween uiTween in uiTweens) {
				uiTween.PlayForward ();
			}
		}
	}

	private void Disable ()
	{
		foreach (UITween uiTween in uiTweens) {
			if (uiTween.tweener != null) {
				//uiTween.tweener.DOKi
			}
		}

		//When quitting the game in editor mode this may be null
		if (this != null) {
			this.gameObject.SetActive (false);
		}
	}
}
