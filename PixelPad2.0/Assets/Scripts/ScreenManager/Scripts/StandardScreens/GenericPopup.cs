﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class GenericPopup : UIScreen
{
	public Text titleLabel;
	public Text descriptionLabel;

	public void Init(string title, string description)
	{
		titleLabel.text = title;
		descriptionLabel.text = description;
	}

}
