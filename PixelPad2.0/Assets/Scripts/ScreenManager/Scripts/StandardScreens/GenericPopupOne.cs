﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class GenericPopupOne : UIScreen
{
	public Text titleLabel;
	public Text descriptionLabel;
	public Text buttonLabel;
	private Action buttonCallback;

	public void Init (string title, string description, string buttonText = "Ok", Action callback = null)
	{
		titleLabel.text = title;
		descriptionLabel.text = description;
		buttonLabel.text = buttonText;
		buttonCallback = callback;
	}

	public void OnButton1Click ()
	{
		ScreenManager.instance.Pop <GenericPopupOne> ();
		if (buttonCallback != null)
			buttonCallback ();
	}

}
