﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class GenericPopupTwo : UIScreen
{
	public Text titleLabel;
	public Text descriptionLabel;
	public Text button1Label;
	public Text button2Label;
	private Action button1Callback;
	private Action button2Callback;

	public void Init (string title, string description, string button1Text = "Cancel", string button2Text = "Ok", Action callback1 = null, Action callback2 = null)
	{
		Debug.Log (string.Format ("GenericPopupTwo: {0}", description));
		titleLabel.text = title;
		descriptionLabel.text = description;
		button1Label.text = button1Text;
		button2Label.text = button2Text;
		button1Callback = callback1;
		button2Callback = callback2;
	}

	public void OnButton1Click ()
	{
		ScreenManager.instance.Pop<GenericPopupTwo> ();
		if (button1Callback != null)
			button1Callback ();
	}

	public void OnButton2Click ()
	{
		ScreenManager.instance.Pop<GenericPopupTwo> ();
		if (button2Callback != null)
			button2Callback ();
	}

}
