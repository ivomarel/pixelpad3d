﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System;

public class UIPositionTween : UITween
{
	public bool isRelative;
	public Vector2 endValue;

	protected override void SetReverse ()
	{
		if (reverse) {
			Vector2 temp = rectTransform.anchoredPosition;
			rectTransform.anchoredPosition = endValue;

			endValue = isRelative ? temp - rectTransform.anchoredPosition : temp;

		}
	}

	public override void PlayForward (Action onCompleted = null)
	{
		bool isNew = tweener == null;
		if (isNew) {
			tweener = rectTransform.DOAnchorPos (endValue, duration);
			tweener.SetRelative (isRelative);
		}
		base.BasePlayForward (isNew, onCompleted);
	}


}
