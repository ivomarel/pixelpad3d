﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System;

[RequireComponent(typeof(CanvasGroup))]
public class UIFadeTween : UITween
{
	public float endValue = 1;

	protected override void SetReverse ()
	{
		if (reverse) {
			float temp = canvasGroup.alpha;
			canvasGroup.alpha = endValue;
			endValue = temp;
		}
	}

	public override void PlayForward (Action onCompleted = null)
	{
		bool isNew = tweener == null;
		if (isNew) {
			tweener = canvasGroup.DOFade (endValue, duration);
		}
		base.BasePlayForward (isNew);
	}

}
