using UnityEngine;
using System.Collections;
using DG.Tweening;
using System;


public abstract class UITween : MonoBehaviour
{
	public float duration = 0.3f;
	public Ease easeType = Ease.InOutSine;
	//public AnimationCurve animationCurve;
	public float delay = 0;
	public LoopType loopType = LoopType.Restart;
	public int loops = 1;
	public bool playOnTransition;
	public bool playOnEnable;

	public bool reverse;
	
	internal Tweener tweener;
	
	protected Canvas canvas {
		get {
			if (_canvas == null) {
				_canvas = FindObjectOfType<Canvas> ();
			}
			return _canvas;
		}
	}

	protected RectTransform rectTransform;
	protected CanvasGroup canvasGroup {
		get {
			if (_canvasGroup == null)
				_canvasGroup = GetComponent<CanvasGroup> ();
			return _canvasGroup;
		}
	}

	private CanvasGroup _canvasGroup;

	private Canvas _canvas;
	private Action onCompleted;
	private Action onRewinded;

	protected virtual void Awake ()
	{
		rectTransform = this.GetComponent<RectTransform> ();
		SetReverse ();
	}

	private void OnEnable ()
	{
		if (playOnEnable) {
			if (tweener != null) {
				tweener.Restart (false);
			}
			PlayForward ();
		}
	}

	protected abstract void SetReverse ();

	public abstract void PlayForward (Action onCompleted = null);

	protected void BasePlayForward (bool isNew, Action onCompleted = null)
	{
		if (isNew) {
			this.onCompleted = onCompleted;
			tweener.SetAutoKill (false);
			tweener.SetEase (easeType);
			tweener.SetDelay (delay);
			tweener.OnRewind (OnTweenerRewinded);
			tweener.OnComplete (OnTweenerComplete);
			tweener.SetLoops (loops, loopType);
		}
		tweener.PlayForward ();
	}

	public virtual void PlayReverse (Action onRewinded = null)
	{
		this.onRewinded = onRewinded;
		tweener.PlayBackwards ();
	}

	private void OnTweenerComplete ()
	{
		if (onCompleted != null)
			onCompleted ();
	}

	private void OnTweenerRewinded ()
	{
		if (onRewinded != null)
			onRewinded ();
	}

}
