﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEngine.UI;

public class PrefabWindow : EditorWindow
{
	[MenuItem ("ScreenManager/Text %#t")]
	private static void CreateText()
	{
		CreateUIObject("Text");
	}

	[MenuItem ("ScreenManager/UIScreen %#u")]
	private static void CreateScreen()
	{
		CreateUIObject("NewScreen");
	}

	[MenuItem ("ScreenManager/Button %#q")]
	private static void CreateButton()
	{
		CreateUIObject("Button");
	}

	[MenuItem ("ScreenManager/InputField %#i")]
	private static void CreateInputField()
	{
		CreateUIObject("InputField");
	}


	private static void CreateUIObject(string name)
	{
		RectTransform rectTransform = Instantiate(Resources.Load<RectTransform>("SM_Prefabs/" + name)) as RectTransform;
		Undo.RegisterCreatedObjectUndo(rectTransform.gameObject, "SM: Instantiate " + name);
		SetParent(rectTransform);
		rectTransform.name = name;
		Selection.activeTransform = rectTransform.transform;
	}

	private static void SetParent(RectTransform rectTransform)
	{
		if (Selection.transforms != null && Selection.transforms.Length == 1)
		{
			Transform transform = Selection.transforms [0];
			if (transform.GetComponentInParent<Canvas>() != null)
			{
				rectTransform.SetParent(transform, false); 
			}
			return;
		} 

		Canvas canvas = GameObject.FindObjectOfType<Canvas>();
		if (canvas != null)
		{
			rectTransform.SetParent(canvas.transform, false);
			return;
		}

		Debug.Log("No Canvas found in scene");
	}
}
