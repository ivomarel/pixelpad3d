﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if CSHARP

class NewCompiler : BaseCompiler
{
    public override BaseObject Compile(ContainerObject cont, LineString line, ref int i)
    {
        NewObject no = CreateCodeObject<NewObject>(cont, line);
        no.name = line.words[++i].word;
        return no;
    }

    public override bool IsType(string name)
    {
        return name == "new";
    }
}

#endif