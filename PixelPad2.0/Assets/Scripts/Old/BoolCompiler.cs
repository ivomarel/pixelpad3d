﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if CSHARP

public class BoolCompiler : VariableCompiler
{
    public override BaseObject Compile(ContainerObject cont, LineString line, ref int i)
    {
        VariableObject obj = CreateVarObject<BoolObject>(cont, line, ref i);
        return obj;
    }

    public override bool IsType(string name)
    {
        return name == "bool";
    }
}

#endif