﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if CSHARP

public class IntCompiler : VariableCompiler
{
    public override BaseObject Compile(ContainerObject cont, LineString line, ref int i)
    {
        VariableObject obj = CreateVarObject<IntObject>(cont, line, ref i);
        //return original i because that's still a referenceObject
        return obj;
    }

    public override bool IsType(string name)
    {
        return name == "int";
    }
}
#endif
