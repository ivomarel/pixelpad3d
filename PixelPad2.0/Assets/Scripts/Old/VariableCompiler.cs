﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if CSHARP
public abstract class VariableCompiler : BaseCompiler
{

    //TODO Why did I only register if they were classes? Weird.
    protected void RegisterIfClassVar(VariableObject vo)
    {
        ContainerObject co = vo.container;
        if (co != null)
        {
            co.AddVar(vo);
        }
    }

    /*
    public VariableObject CreateVarObject<T>(ContainerObject cont, LineString line, ref int i) where T : VariableObject, new()
    {
        VariableObject obj = null;
        if (line.words.Length > i + 1 && line.words[i + 1].word == "[" && line.words[i + 2].word == "]") {
            obj = CreateCodeObject<ArrayObject<T>>(cont, line);
            //Skip the brackets
            i += 2;
        } else {
            obj = CreateCodeObject<T>(cont, line);
        }
        obj.name = line.words[i + 1].word;
        //Important that every variablecompiler also registers
        RegisterIfClassVar(obj);
        return obj;
    }
    */
}
#endif