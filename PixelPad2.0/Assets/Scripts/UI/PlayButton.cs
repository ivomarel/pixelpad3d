﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayButton : MonoBehaviour {

    private Text playText;
	// Use this for initialization
	void Awake () {
        playText = GetComponentInChildren<Text>();
        UpdateText();
    }
	
	// Update is called once per frame
	void Update () {
        UpdateText();

    }

    void UpdateText ()
    {
        playText.text = Engine.instance.isPlaying ? "Stop" : "Play";
    }
}
