﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectsScreen : UIScreen {

    public LoadProjectButton loadProjectButtonOriginal;


    private void Awake()
    {
        loadProjectButtonOriginal.gameObject.SetActive(false);
    }

    IEnumerator Start()
    {
        yield return new WaitUntil(() => ParseManager.instance.isAuthorized);
        RefreshProjects();
    }

    public override void OnReturn(UIScreen fromScreen)
    {
        base.OnReturn(fromScreen);
        if (ParseManager.instance.isAuthorized)
            RefreshProjects();
    }


    public void OnRefreshButton()
    {
        RefreshProjects();
    }

    public void OnNewProjectsButton () 
    {
        EngineScreen es = ScreenManager.instance.Push<EngineScreen>();
        es.ClearProjectData();
    }

    void RefreshProjects()
    {
        ClearProjectButtons();
        JSONExporter.LoadProjects(OnProjectsLoaded);
    }

    void OnProjectsLoaded(Dictionary<string, ProjectData> projects)
    {
        ClearProjectButtons();
        foreach (var project in projects)
        {
            LoadProjectButton loadProjectButtonClone = Instantiate(loadProjectButtonOriginal, loadProjectButtonOriginal.transform.parent);
            loadProjectButtonClone.gameObject.SetActive(true);
            loadProjectButtonClone.Init(project.Key, project.Value);
        }
    }

    void ClearProjectButtons()
    {
        foreach (LoadProjectButton l in loadProjectButtonOriginal.transform.parent.GetComponentsInChildren<LoadProjectButton>())
        {
            Destroy(l.gameObject);
        }
    }

    public void OnLoadProjectButton(LoadProjectButton loadProjectButton)
    {
        EngineScreen es = ScreenManager.instance.Push<EngineScreen>();
        es.SetProjectData(loadProjectButton.projectName, loadProjectButton.projectData);
    }

    public void OnDeleteProjectButton(LoadProjectButton loadProjectButton)
    {
        loadProjectButton.SetInteractable(false);
        var deleteProject = new GameSparks.Api.Requests.LogEventRequest_DeleteProject();
        deleteProject.Set_Name(loadProjectButton.projectName).Send((obj) =>
        {
            //Success
            Destroy(loadProjectButton.gameObject);
        },(obj) => {
            //Error
            loadProjectButton.SetInteractable(true);
        });

    }
}
