﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class EngineScreen : UIScreen {

    public InputField levelNameField;

    public RectTransform[] playModeCovers;

    internal TabPanel tabPanel;

    void Awake()
    {
        tabPanel = GetComponentInChildren<TabPanel>();
    }


    public void OnPlayButton()
    {
        Engine.instance.TogglePlayMode();
        foreach(RectTransform rt in playModeCovers)
        {
            rt.gameObject.SetActive(Engine.instance.isPlaying);
        }
        EventSystem.current.SetSelectedGameObject(null);
    }

    public void OnSaveButton ()
    {
        ProjectData data = GetProjectData();
        JSONExporter.SaveProject(levelNameField.text, data, OnProjectSaved);
    }

    public void OnQuitButton () {
        ScreenManager.instance.Pop();
    }

    void OnProjectSaved ()
    {
        // For some reason this did not get the last project. 
        // RefreshProjects();
    }

    public ProjectData GetProjectData()
    {
        ProjectData projectData = new ProjectData();
        projectData.classes = new List<ClassData>();
        foreach (TabToggle tab in tabPanel.scriptToggles)
        {
            ClassData classData = new ClassData();
            classData.name = tab.scriptName;
            classData.script = tab.scriptField.scriptText;
            projectData.classes.Add(classData);
        }

        return projectData;
    }

    public void ClearProjectData () {
        tabPanel.Clear();
    }

    public void SetProjectData(string name, ProjectData projectData)
    {
        ClearProjectData();
        levelNameField.text = name;
        foreach (ClassData classData in projectData.classes)
        {
            tabPanel.AddScript(classData.name, classData.script);
        }
    }
}
