﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TabPanel : MonoBehaviour {

    public TabToggle tabButtonOriginal;
    public ScriptField textFieldOriginal;

    internal List<TabToggle> scriptToggles = new List<TabToggle>();

    string defaultScript = "def Start():\n\t#Use this for initialization\n\ndef Update():\n\t#Update is called once per frame";

    private void Awake()
    {
        tabButtonOriginal.gameObject.SetActive(false);
        textFieldOriginal.gameObject.SetActive(false);
    }

    public TabToggle SelectScript (string scriptName) {
        foreach(TabToggle toggle in scriptToggles) {
            if (toggle.scriptName == scriptName) {
                toggle.SelectTab();
                return toggle;
            }
        }

        return null;
    }

    public void OnPlusButton ()
    {
        AddScript("NewClass", defaultScript);
        TabToggle newToggle = scriptToggles[scriptToggles.Count - 1];
        newToggle.SelectTab();
        newToggle.SelectForNameChange();
    }

    public void AddScript (string name, string script)
    {
        ScriptField textFieldClone = Instantiate(textFieldOriginal, textFieldOriginal.transform.parent);
        //textFieldClone.gameObject.SetActive(true);
        textFieldClone.Init(script);

        TabToggle tabButtonClone = Instantiate(tabButtonOriginal, tabButtonOriginal.transform.parent);
        tabButtonClone.gameObject.SetActive(true);
        tabButtonClone.Init(name, textFieldClone);
        
        scriptToggles.Add(tabButtonClone);
    }

    public void RemoveScript (TabToggle scriptObj)
    {
        scriptToggles.Remove(scriptObj);
        Destroy(scriptObj.scriptField.gameObject);
        Destroy(scriptObj.gameObject);
    }

    public void Clear ()
    {
        foreach (TabToggle tab in scriptToggles)
        {
            Destroy(tab.scriptField.gameObject);
            Destroy(tab.gameObject);
        }
        scriptToggles.Clear();
    }
   
}
