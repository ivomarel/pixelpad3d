﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class ConsoleBlock : MonoBehaviour {

    public Text errorText;
    public Image errorImage;
    public ErrorImageColor[] colors;

    private LogInfo info;

    public void Init(LogInfo info)
    {
        this.info = info;
        string consoleText = string.Format("{0}: line {1}: {2}", info.script, info.line, info.message);
        errorText.text = consoleText;
        foreach(ErrorImageColor c in colors) {
            if (c.type == info.type) {
                errorImage.color = c.color;
                break;
            }
        }
       
    }

    public void OnClick () {
        TabPanel panel = ScreenManager.instance.GetScreenInstance<EngineScreen>().tabPanel;
        var toggle = panel.SelectScript(info.script);
        StartCoroutine(SetCaretPos(toggle));
    }

    IEnumerator SetCaretPos (TabToggle toggle)
    {
        //We wait a frame for the script to be selected
        yield return null;
        //TEMP not setting caret to proper word just yet
        toggle.scriptField.SetCaretPosition(info.line, 0);
    }
}

[System.Serializable]
public class ErrorImageColor {
    public Console.LogType type;
    public Color color;
}

