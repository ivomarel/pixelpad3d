﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TabToggle : MonoBehaviour, IPointerClickHandler {

    internal string scriptName
    {
        get
        {
            return nameField.text;
        }
        set
        {
            nameField.text = value;
        }
    }
    internal ScriptField scriptField;

    private Toggle toggle;
    private InputField nameField;


    void Awake ()
    {
        toggle = GetComponent<Toggle>();
        nameField = GetComponentInChildren<InputField>();
    }

	public void Init(string name, ScriptField scriptField)
    {
        this.scriptField = scriptField;
        scriptName = name;
        toggle.onValueChanged.AddListener(OnToggleChange);
    }

    void OnToggleChange(bool value)
    {
        scriptField.gameObject.SetActive(value);
    }

    int tap;

    public void OnDeleteButton ()
    {
        GetComponentInParent<TabPanel>().RemoveScript(this);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        tap = eventData.clickCount;
        switch (tap)
        {
            case 1:
                SelectTab();
                break;
            case 2:
                SelectForNameChange();
                break;
            default:
                break;
        }


    }

    public void SelectTab ()
    {
        if (!toggle.isOn)
            toggle.isOn = true;
    }

    public void SelectForNameChange ()
    {
        nameField.interactable = true;
        nameField.Select();
    }
}
