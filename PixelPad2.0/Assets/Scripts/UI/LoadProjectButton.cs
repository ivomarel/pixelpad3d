﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadProjectButton : MonoBehaviour {

    public Text projectNameText;
    public Image isInteractableImage;

    internal string projectName;
    internal ProjectData projectData;

    public void Init(string name, ProjectData data)
    {
        this.projectName = name;
        this.projectData = data;
        projectNameText.text = name;
    }

    public void SetInteractable(bool value) {
        isInteractableImage.gameObject.SetActive(!value);
    }
}
