﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class AssetObject : MonoBehaviour {

    public Text assetNameLabel;
    public Text assetInfoLabel;

    public void Init(string assetName, string info)
    {
        assetNameLabel.text = assetName;
        assetInfoLabel.text = info;
    }
}
