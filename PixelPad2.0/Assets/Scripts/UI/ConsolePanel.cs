﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConsolePanel : MonoBehaviour {

    public ConsoleBlock blockOriginal;

    bool clearOnPlay;

    private void Awake()
    {
        blockOriginal.gameObject.SetActive(false);
        Console.onLog += Console_OnLog;
        Engine.onPlayModeSwitch += Engine_onPlayModeSwitch;
        gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        
    }

    //private void OnDisable()
    //{
    //    Console.onLog -= Console_OnLog;
    //    Engine.onPlayModeSwitch -= Engine_onPlayModeSwitch;
    //}

    void Console_OnLog(LogInfo info)
    {
        ConsoleBlock blockClone = Instantiate(blockOriginal, blockOriginal.transform.parent);
        blockClone.gameObject.SetActive(true);
        blockClone.Init(info);
    }

    private void Engine_onPlayModeSwitch(bool isPlaying)
    {
        if (clearOnPlay && isPlaying)
        {
            Clear();
        }
    }

    public void Clear ()
    {
        foreach(ConsoleBlock block in GetComponentsInChildren<ConsoleBlock>())
        {
            Destroy(block.gameObject);
        }
    }

    public void ClearOnPlay (bool value)
    {
        clearOnPlay = value;
    }

}
