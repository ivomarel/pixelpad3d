﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Formula  {

    List<ModifyObject> formula = new List<ModifyObject>();

    ValueObject result;

    public void Add (ModifyObject mo)
    {
        if (mo != null && mo.formula == null)
        {
            mo.formula = this;
            formula.Add(mo);
        }
    }

    public ValueObject GetValue ()
    {
        foreach(ModifyObject mo in formula)
        {
            mo.AssignValues();
        }

        result = null;

        //Because -something is calculated before anything else.
        AssignRightHandValues<DeductObject>();

        CalculateValues<MultiplyObject>();
        CalculateValues<AdditiveObject>();
        CalculateValues<DeductObject>();
        CalculateValues<CompareObject>();
        ClearFormula();

        //We assign the final result of additive to the left-hand side of the = statement

        return result;
    }


    void ClearFormula()
    {
        foreach (ModifyObject mo in formula)
        {
            mo.result = null;
            mo.formula = null;
        }
        formula.Clear();
    }

    void AssignRightHandValues<T>() where T : ModifyObject {
        
        for (int i = 0; i < formula.Count; i++)
        {
            ModifyObject mo = formula[i];
            //We cast it the the Generic type, e.g. we check if this is a multiplyobject or not
            T tMo = mo as T;
            if (tMo != null)
            {
                //If it has no left-hand var, it must be a right hand var.
                if (mo.GetAdjacentValueObject(0) == null) {
                    mo.result = mo.GetValueObject();
                }
            }
        }
    }

    ValueObject CalculateValues<T>() where T : ModifyObject
    {
        for (int i = 0; i < formula.Count; i++)
        {
            ModifyObject mo = formula[i];

            //We cast it the the Generic type, e.g. we check if this is a multiplyobject or not
            T tMo = mo as T;
            //If the object is null and has not been modified yet
            if (tMo != null && tMo.result == null)
            {
                //If there was a * modifier, a later + modifier should use the result of the * modifier
                if (i > 0)
                {
                    //We get the next or previous modifier
                    ModifyObject prevModify = GetAdjacentModifyObject(i, -1);
                    if (prevModify.result != null)
                    {
                        mo.SetValueObj(0, prevModify.result);
                    }
                }
                //We check both the left and the right side
                //E.g. 7 + 4 * 3. The result of 4*3 is stored in the MultiplyObject. Then we don't add 7 + 4, but 7 + 12 instead. Resulting finally in 19
                if (i < formula.Count - 1)
                {
                    //We get the next or previous modifier
                    ModifyObject nextModify = GetAdjacentModifyObject(i, 1);
                    if (nextModify.result != null)
                    {
                        mo.SetValueObj(1, nextModify.result);
                    }
                }

                tMo.Modify();
                result = tMo.result;

                //We store the results in adjacent modify-objects
                //We do this for all adjacent that already have results
                int j = i + 1;
                while (j < formula.Count)
                {
                    ModifyObject adjacentMo = formula[j];
                    if (adjacentMo == null || adjacentMo.result == null)
                        break;

                    adjacentMo.result = result;
                    j++;
                }

                j = i - 1;
                while (j > 0)
                {
                    ModifyObject adjacentMo = formula[j];
                    if (adjacentMo == null || adjacentMo.result == null)
                        break;

                    adjacentMo.result = result;
                    j--;
                }

            }
        }
       
        return result;
    }

    ModifyObject GetAdjacentModifyObject (int i, int dir) {
        i += dir;
        while (i < formula.Count && i >= 0) {
            ModifyObject mo = formula[i] as ModifyObject;
            if (mo != null) {
                return mo;
            }
            i += dir;
        }
        return null;
    }


}
