﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System.IO;
using System;

public class JSONExporter {

    static string PROJECT_KEY = "Project2";

    static bool localSave;

    static Action<Dictionary<string, ProjectData>> successCallback;
    static Action saveSuccessCallback;


    static bool isSaving;

    public static void SaveProject (string name, ProjectData data, Action succesCallback)
    {
        if (isSaving)
            return;

        localSave = false;

        saveSuccessCallback = succesCallback;
        isSaving = true;
        string dataString = JsonConvert.SerializeObject(data);
        Debug.Log("Saving as " + dataString);
        if (localSave)
        {
            PlayerPrefs.SetString(PROJECT_KEY, dataString);
            isSaving = false;
        } else
        {
            ParseManager.instance.SaveLevel(name, dataString, () =>
            {
                Debug.Log("Save success!");
                isSaving = false;
                if (saveSuccessCallback != null)
                    saveSuccessCallback();
            });
        }
    }

    public static void LoadProjects (Action<Dictionary<string, ProjectData>> callback)
    {
        Debug.Log("Loading projects..");
        successCallback = callback;
        if (localSave)
        {
            Dictionary<string, ProjectData> projects = new Dictionary<string, ProjectData>();

            if (PlayerPrefs.HasKey(PROJECT_KEY))
            {
                string dataString = PlayerPrefs.GetString(PROJECT_KEY);

                ProjectData data = JsonConvert.DeserializeObject<ProjectData>(dataString);
                projects.Add(PROJECT_KEY, data);
            }
            if (successCallback != null)
                successCallback(projects);
        } else
        {
            ParseManager.instance.FetchLevels(OnProjectsFetched);
        } 
    }

    static void OnProjectsFetched (string json)
    {
        Debug.Log(json);
        ProjectsData data = JsonConvert.DeserializeObject<ProjectsData>(json);
        Dictionary<string, ProjectData> projects = new Dictionary<string, ProjectData>();
        foreach(var pc in data.projects)
        {
            ProjectData pData = JsonConvert.DeserializeObject<ProjectData>(pc.Value.data);
            projects.Add(pc.Key, pData);
        }

        if (successCallback != null)
            successCallback(projects);
    }
}

public class ProjectsData
{
    public Dictionary<string, ProjectContainer> projects;
}

public class ProjectContainer
{
    public string data;
}

public class ProjectData
{
    public List<ClassData> classes = new List<ClassData>();
}

public class ClassData
{
    public string name;
    public string script;
}
