﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;



//Should this be a singleton?
public class Console : Singleton<Console> {

    public enum LogType
    {
        Regular,
        Warning,
        Error
    }

    public static event Action<LogInfo> onLog;

    public static void Log (LogInfo info) {
        if (onLog != null)
            onLog(info);
    }
}


public struct LogInfo
{
    public int line;
    public string script;
    public object message;
    public Console.LogType type;
}