﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefaultBehaviour : MonoBehaviour {

    public ClassObject c;

    int x = 0;
    int y = 0;
    string sprite = "";

    SpriteRenderer sr;

    void Awake ()
    {
        gameObject.transform.localScale = new Vector3(10, 10);

        Sprite defaultSprite = Resources.Load<Sprite>("Art/EmptySprite");
        SetSprite(defaultSprite);
    }

    // Use this for initialization
    void Start () {
        RunFunction("Start");
    }
	
	// Update is called once per frame
	void Update () {
        RunFunction("Update");
        ProcessVariables();
    }

    void RunFunction(string functionName)
    {
        FunctionObject f;
        if (c.functions.TryGetValue(functionName, out f))
        {
            f.Execute();
        }
    }

    T GetVar<T>(string name)
    {
        return (T)c.GetVar(name).GetValueObject().GetValue();
    }

    void ProcessVariables ()
    {
        x = GetVar<int>("x");
        y = GetVar<int>("y");

        transform.position = new Vector3(x, y);

        sprite = GetVar<string>("sprite");
        if (sprite != "")
        {
            Sprite sp;
            if (AssetManager.instance.sprites.TryGetValue(sprite, out sp))
            {
                SetSprite(sp);
            }
        }

    }

    void SetSprite (Sprite sprite)
    {
        if (sr == null)
        {
            sr = GetComponent<SpriteRenderer>();
            if (sr == null)
            {
                sr = gameObject.AddComponent<SpriteRenderer>();
            }
        }
        sr.sprite = sprite;
    }
}
