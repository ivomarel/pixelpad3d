﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssetManager : Singleton<AssetManager> {

    public Dictionary<string, Sprite> sprites = new Dictionary<string, Sprite>();
}
