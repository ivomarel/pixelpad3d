﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class Engine : Singleton<Engine> {

    public bool isPlaying;

    public static event Action<bool> onPlayModeSwitch;

    bool isLoading;

    private const string GAME_SCENE = "GameScene";

    private void Start()
    {
        StartCoroutine(ReloadGameScene());
    }

    public void TogglePlayMode ()
    {
        SetPlayMode(!isPlaying);
    }

    public void SetPlayMode(bool value)
    {
        if (isLoading)
        {
            Debug.Log("Currently changing playmode. Please wait.");
            return;
        }

        isLoading = true;
        if (value == isPlaying)
            return;

        isPlaying = value;

        if (onPlayModeSwitch != null)
            onPlayModeSwitch(value);

        if (isPlaying)
        {
            new GameObject("MainCompiler").AddComponent<MainCompiler>();
            MainCompiler.instance.Compile(ScreenManager.instance.GetScreenInstance<EngineScreen>().GetProjectData());
            MainCompiler.instance.Play();
            isLoading = false;
        } else
        {
            StartCoroutine(ReloadGameScene());
        }
    }

    IEnumerator ReloadGameScene ()
    {
        if (SceneManager.GetSceneByName(GAME_SCENE).isLoaded)
        {
            AsyncOperation asop = SceneManager.UnloadSceneAsync(GAME_SCENE);
            while (!asop.isDone)
            {
                yield return null;
            }
        }
        SceneManager.LoadScene(GAME_SCENE, LoadSceneMode.Additive);
        Scene gameScene = SceneManager.GetSceneByName(GAME_SCENE);
        while (!gameScene.isLoaded)
            yield return null;
        SceneManager.SetActiveScene(gameScene);

        isLoading = false;
    }
}
