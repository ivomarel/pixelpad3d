﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Not to be inherited, because the RefVariable will be defined for any variable.
/// Rather than storing in any RefVariable what type of variable it is, RefVariables reference the original declared variable
/// For magic numbers, we simply create an original VariableObject inside that is passed on
/// </summary>
public class ReferenceObject : BaseObject
{

    public VariableObject variable;

    //References can either references a variable OR directly a value.
    public ValueObject value;

    //We store the name only for debugging now
    public string name;

    /// <summary>
    /// Returns the referenced value
    /// </summary>
    /// <returns></returns>
    public override ValueObject GetValueObject()
    {
        int i = index + 1;

        if (i < line.objs.Count)
        {
            //If there is a modifier on the right side, we should calculate the formula
            ModifyObject mo = line.objs[i] as ModifyObject;
            if (mo != null && mo.formula == null)
            {
                Formula f = new Formula();
                do
                {
                    mo = line.objs[i] as ModifyObject;
                    if (mo != null && mo.formula == null)
                    {
                        f.Add(mo);
                    }
                    i++;
                } while (i < line.objs.Count);

                return f.GetValue();
            }
            IndexObject indexObj = line.objs[i] as IndexObject;
            if (indexObj != null)
            {
                BaseObject r = line.objs[i + 1];
                ValueObject valueObj = r.GetValueObject();
                //Should be an intobject, index of array
                IntObject intObj = valueObj as IntObject;
                if (intObj != null)
                {
                    ValueObject array = GetVariable().GetValueObject();
                    ReferenceObject objAtIndex = array.GetIndex(intObj);
                    //Get a reference to the object in this array!
                    return objAtIndex.GetValueObject();
                } 
            }

        }

        //Some objects don't link to a variable (e.g. a nr like '3')
        if (value != null)
            return value;

        return GetVariable().GetValueObject();
    }

    public VariableObject GetVariable()
    {
        //This CAN NOT be done on compilation
        //That's because we are accessing other objects that wouldn't exist yet in compilation


        //Remvoing null check because when a variable is reassigned this doesn't work.
        //markedForDeletion is not working currently so just leaving this unoptomized
        //TODO this might need to be optimized!!! 
        //if (variable == null || variable.markedForDeletion)
        {
            //Ensuring null if it was marked for deletion
            variable = null;
           // value = null;

            if (name.Contains("."))
            {
                variable = GetVariableFromOtherObject(name);
                //Debug.LogFormat("Got variable {0} with value {1}", variable.name, variable.GetValue());
            }
            else
            {
                if (container == null)
                {
                    Debug.LogWarningFormat("Variable {0} has no container. Woops?", name);
                }
                //This should happen only once when the variable hasn't been linked yet
                variable = container.GetVar(name);
            }

            if (variable == null)
                Debug.LogErrorFormat("Variable {0} reference is null. This should never happen", name);
        }

        return variable;
    }

    VariableObject GetVariableFromOtherObject(string name)
    {
        ContainerObject curr = container;
        string[] parts = name.Split('.');
        for (int j = 0; j < parts.Length; j++)
        {
            string part = parts[j];
            if (!curr.HasVar(part))
            {
                Debug.LogWarningFormat("Trying to get value {0} from {1}, but this variable doesn't exist", part, curr.GetString());
                break;
            }
            //Value can be an actual value like and int or something, but could also be another container
            VariableObject var = curr.GetVar(part);

            if (var == null)
            {
                Debug.LogWarningFormat("Variable {0} in {1} of {2} is null", part, curr.GetType(), name);
            }

            if (j == parts.Length - 1)
            {
                return var;
            }

            //If it wasn't the last part (e.g. 'position' in myObject.position.x), keep going deeper
            ValueObject value = var.GetValueObject();
            ContainerObject c = value.GetValue() as ContainerObject;
            if (c != null)
            {
                curr = c;
            }
        }

        Debug.LogWarningFormat("Trying to get variable {0}, but it failed for unknown reason.", name);
        return null;
    }

}
