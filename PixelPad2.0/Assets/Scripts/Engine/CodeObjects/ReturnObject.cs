﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnObject : ExecutableObject {

    public VariableObject returnVar;

    protected override void OnExecute()
    {
        BaseObject returnObject = null;

        if (line.objs.Count > index+1)
            returnObject = line.objs[index + 1] as BaseObject;

        FunctionObject fo = container as FunctionObject;

        if (fo != null)
            fo.OnReturn(returnObject);
        else
            Debug.LogWarningFormat("Error message at {0} ", line.ToString());
    }
}
