﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FunctionObject : ContainerObject {

    public string name;
    
    public ValueObject returnObject;

    public List<ReferenceObject> parameters;

    public void ConnectParams ()
    {
        GroupObject go = line.objs[index + 1] as GroupObject;
        parameters = new List<ReferenceObject>();
        foreach (BaseObject b in go.GetGroupObjects())
        {
            parameters.Add(b as ReferenceObject);
        }
    }

    public bool AssignParameters (List<ValueObject> sendParams)
    {
        int nParams = parameters.Count;
        if (nParams != sendParams.Count)
        {
            Debug.LogWarningFormat("Function {0} is expecting {1} parameters but is only receiving {2}", name, nParams, sendParams.Count);
            return false;
        }

        //Assigning the actual parameters
        for (int i = 0; i < nParams; i++)
        {
            //Debug.LogFormat("Assigning {0} to {1}", parameters[i].name, sendParams[i].GetValue());
            parameters[i].GetVariable().SetValueFromVariable(sendParams[i]);
        }

        return true;
    }

    protected override void OnExecute()
    {
        base.OnExecute();
        ClassObject co = container.GetClass();
        //Some default functions like getkey etc.
        if (co.defaultFuncs.ContainsKey(name))
        {
            returnObject = co.CallFunction(this);
        }
    }

    //Called by the ReturnObject
    public void OnReturn (BaseObject referenceObject)
    {
        executionHalted = true;
        if (referenceObject != null)
            returnObject = referenceObject.GetValueObject();
    }

    //When this is used in e.g. AssignObject, we execute the function and return it's result
    public override ValueObject GetValueObject()
    {
        return returnObject;
    }
}
