﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseObject  {

    public ContainerObject container;

    public LineString lineString;
    public int stringIndex;

    public Line line;
    public int index;

    public virtual BaseObject GetClone ()
    {
        return (BaseObject)MemberwiseClone();
    }

    /// <summary>
    ///
    /// </summary>
    /// <returns> Null by default, but various objects, such as functions, references and NewObjects will return something else</returns>
    public virtual ValueObject GetValueObject ()
    {
        return null;
    }

    public virtual string GetString ()
    {
        return GetType().ToString();
    }

    public string GetOriginalWord ()
    {
        if (lineString != null && stringIndex < lineString.words.Length)
        {
            return lineString.words[stringIndex].word;
        }

        return "---";
    }
}
