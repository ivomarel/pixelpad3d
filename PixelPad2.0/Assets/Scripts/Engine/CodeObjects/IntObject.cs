﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntObject : ValueObject  {

    public int value;

    public override object GetValue()
    {
        return value;
    }

    public override void SetValue(object v)
    {
        value = (int)v;
    }

    public override ValueObject Add(ValueObject otherObject)
    {
        IntObject result = new IntObject();

        IntObject obj = (IntObject)otherObject;
        if (obj != null)
        {
            result.value = value + obj.value;
            return result;
        }
        //Error message
        return base.Add(otherObject);
    }

    public override ValueObject Deduct(ValueObject otherObject)
    {
        IntObject result = new IntObject();

        IntObject obj = (IntObject)otherObject;
        if (obj != null)
        {
            result.value = value - obj.value;
            return result;
        }
        //Error message
        return base.Deduct(otherObject);
    }

    public override ValueObject Multiply(ValueObject otherObject)
    {
        IntObject result = new IntObject();

        IntObject obj = (IntObject)otherObject;
        if (obj != null)
        {
            result.value = value * obj.value;
            return result;
        }
        //Error message
        return base.Multiply(otherObject);
    }

    public override ValueObject Minus()
    {
        IntObject result = new IntObject();
        result.value = -value;
        return result;
    }

    public override BoolObject Compares(CompareType type, ValueObject otherObject)
    {
        BoolObject result = new BoolObject();

        IntObject obj = (IntObject)otherObject;
        if (obj != null)
        {
            switch (type)
            {
                case CompareType.Equal:
                    result.value = value == obj.value;
                    break;
                case CompareType.NotEqual:
                    result.value = value != obj.value;
                    break;
                case CompareType.Greater:
                    result.value = value > obj.value;
                    break;
                case CompareType.Less:
                    result.value = value < obj.value;
                    break;
            }
            
            return result;
        }

        return base.Compares(type, otherObject);
    }
}
