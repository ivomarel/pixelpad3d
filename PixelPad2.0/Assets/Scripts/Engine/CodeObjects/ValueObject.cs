﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ValueObject : BaseObject {

    public abstract object GetValue();

    public abstract void SetValue(object v);

    /*
    public void SetValueFromVariable(ValueObject v)
    {
        SetValue(v.GetValue());
    }
    */

        //To avoid dumb mistakes where we get the value from the value
    public override ValueObject GetValueObject()
    {
        return this;
    }

    public virtual ValueObject Add(ValueObject otherObject)
    {
        Debug.LogWarningFormat("Objects of the type {0} have no add implementation", GetType());
        return null;
    }

    public virtual ValueObject Multiply(ValueObject otherObject)
    {
        Debug.LogWarningFormat("Objects of the type {0} have no multiply implementation", GetType());
        return null;
    }

    public virtual ValueObject Deduct(ValueObject otherObject)
    {
        Debug.LogWarningFormat("Objects of the type {0} have no multiply implementation", GetType());
        return null;
    }

    public virtual ValueObject Minus()
    {
        Debug.LogWarningFormat("Objects of the type {0} have no minus implementation", GetType());
        return null;
    }

    public virtual BoolObject Compares (CompareType type, ValueObject otherObject)
    {
        Debug.LogWarningFormat("Objects of the type {0} can not equal", GetType());
        return null;
    }

    public virtual ReferenceObject GetIndex (IntObject i)
    {
        return null;
    }
}
