﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiplyObject : ModifyObject
{
    protected override int priority
    {
        get
        {
            throw new System.NotImplementedException();
        }
    }

    public override void Modify()
    {
        result = GetAdjacentValueObject(0).Multiply(GetAdjacentValueObject(1));
    }
}
