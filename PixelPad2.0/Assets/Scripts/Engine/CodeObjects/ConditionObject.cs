﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConditionObject : ContainerObject {

    protected override void OnExecute()    {
        if (ConditionTrue())
        {
            base.OnExecute();
            //variables.Clear();
        } 
    }

    bool ConditionTrue () {
        BaseObject refObj = line.objs[index + 1];
        ValueObject vo = refObj.GetValueObject();
        BoolObject bo = vo as BoolObject;
        if (bo != null)
        {
            return bo.value;
        }
        Debug.LogWarning("This condition is not working. Defaulting to false");
        return false;
    }

}
