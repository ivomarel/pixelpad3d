﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrintObject : ExecutableObject {


    protected override void OnExecute()
    {
        BaseObject refObj = (BaseObject)line.objs[index + 1];

        ValueObject vo = refObj.GetValueObject();
        LogInfo info = new LogInfo()
        {
            script = container.GetClass().name,
            line = line.nr,
            type = Console.LogType.Regular,
            message = vo.GetValue()
        };

        Console.Log(info);
    }
}
