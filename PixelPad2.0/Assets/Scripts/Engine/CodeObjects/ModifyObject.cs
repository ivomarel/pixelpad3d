﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ModifyObject : BaseObject
{

    //Always just uses two values. Value[0] is left, value[1] is right
    private ValueObject[] values = new ValueObject[2];

    //The reult of this modifier
    public ValueObject result;
    public Formula formula;

    //Deprecated?
    protected abstract int priority { get; }

    public void AssignValues()
    {
        AssignValue(0);
        AssignValue(1);
    }

    public void AssignValue(int i)
    {
        //i=0 is the lefthand var, i=1 is righthand var
        int offset = i == 0 ? -1 : 1;
        BaseObject bo = line.objs[index + offset];
        ReferenceObject ro = bo as ReferenceObject;
        if (ro != null) {
            SetValueObj(i, ro.GetValueObject());
            return;
        }

        //For rightside, check if there's a modifier. this way e.g. --3 becomes '3'
        if (offset == 1)
        {
            ModifyObject mo = bo as ModifyObject;
            if (mo != null)
            {
                SetValueObj(i, mo.GetValueObject());
                return;
            }
        } else {
            return;
        }

        Debug.LogWarningFormat("Could not assign {0} of modifier {1}", offset == -1 ? "lefthand" : "righthand", i);

       // ValueObject vo = ro == null ? null : ro.GetValueObject();

    }

    public void SetValueObj(int i, ValueObject obj)
    {
        values[i] = obj;
    }

    public ValueObject GetAdjacentValueObject(int index)
    {
        if (values[index] == null)
        {
            AssignValue(index);
        }
        return values[index];
    }
    /// <summary>
    /// Do the math (e.g. multiply, add)
    /// </summary>
    public abstract void Modify();
        
    /// <summary>
    /// When GetValueObject is called on a Modifier, it will attempt to modify the variable on the right side
    /// </summary>
    public override ValueObject GetValueObject()
    {
        Debug.LogFormat("Objects of type {0} can not be used as right-hand modifiers only", GetType());
        return base.GetValueObject();
    }
}
