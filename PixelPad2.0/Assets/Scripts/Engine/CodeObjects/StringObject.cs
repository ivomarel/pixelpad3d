﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StringObject : ValueObject  {

    public string value;

    public override object GetValue()
    {
        return value;
    }

    public override void SetValue(object v)
    {
        value = (string)v;
    }

    public override ValueObject Add(ValueObject otherObject)
    {
        StringObject result = new StringObject();
        result.value = value + otherObject.GetValue().ToString();
        //Error message
        return result;
    }

    //TODO Getting Index from String?
    /*
    public override ReferenceObject GetIndex(IntObject i)
    {
        return new StringObject()
        {
            value = value[i.value].ToString()
        };
    }
    */
}
