﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CallFunctionObject : ExecutableObject
{
    public FunctionObject functionRef;
    public List<ValueObject> parameters;

    protected override void OnExecute()
    {
        GroupObject go = line.objs[index + 1] as GroupObject;
        parameters = new List<ValueObject>();
        foreach (BaseObject b in go.GetGroupObjects())
        {
            parameters.Add(b.GetValueObject());
        }

        bool paramSuccess = functionRef.AssignParameters(parameters);

        //If there's any issue with the parameters we shouldn't run this function.
        if (paramSuccess)
        {
            functionRef.Execute();
        } else
        {
            Debug.LogErrorFormat("Couldn't execute function {0} due to some issue.", functionRef.name);
        }
    }

    public override ValueObject GetValueObject()
    {
        //This should execute itself to ensure the parameters are sent!
        Execute();
        return functionRef.GetValueObject();
    }
}
