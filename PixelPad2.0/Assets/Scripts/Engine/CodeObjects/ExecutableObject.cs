﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ExecutableObject : BaseObject {

    protected bool isExecuted;

    public void Execute() {
        if (isExecuted)
            return;
        isExecuted = true;
        try
        {
            OnExecute();
        } catch (System.Exception e){
            Debug.LogErrorFormat("Execution error in {0}, {1}, {2}", GetType(), lineString, e);
        }
        //Debug.LogFormat("Finished executing {0}", GetType());
    }

    /// <summary>
    /// We clear isExecuted bools. Those bools are simply to avoid one execution running twice in the same line.
    /// </summary>
    public virtual void Refresh ()
    {
        isExecuted = false;
    }

    protected abstract void OnExecute();
}
