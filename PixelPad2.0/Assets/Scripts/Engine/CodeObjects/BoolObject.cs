﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoolObject : ValueObject  {
    
    public bool value;

    public override object GetValue()
    {
        return value;
    }

    public override void SetValue(object v)
    {
        value = (bool)v;
    }

}
