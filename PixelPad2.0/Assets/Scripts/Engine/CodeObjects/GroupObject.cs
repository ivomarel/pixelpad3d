﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupObject : BaseObject {

    public bool isOpening;
    public GroupObject connectedObj;

    List<BaseObject> objs;

    public override ValueObject GetValueObject()
    {
        List<BaseObject> bos = GetGroupObjects();
        if (bos.Count == 1)
        {
            return bos[0].GetValueObject();
        }
        return null;
    }

    public List<BaseObject> GetGroupObjects ()
    {
        //Faster the second time
        if (objs != null)
            return objs;

        //If it's the ending bracket for some reason, we access the opening bracket like this
        if (!isOpening)
        {
            return connectedObj.GetGroupObjects();
        }

        //We only get the objects at the start and after commas
        bool getNextObject = true;
        objs = new List<BaseObject>();
        for (int i = index + 1; i < connectedObj.index; i++)
        {
            BaseObject obj = line.objs[i];
            //If it's our closing bracket, we skip it.
            if (obj == connectedObj)
                continue;

            //TEMP 
            GroupObject go = obj as GroupObject;
            if (go != null)
                continue;

            SeparatorObject sObj = obj as SeparatorObject;
            if (sObj != null)
            {
                getNextObject = true;
            }
            else
            {
                if (getNextObject)
                {
                    objs.Add(obj);
                    getNextObject = false;
                }
            }
        }

        return objs;
    }

}
