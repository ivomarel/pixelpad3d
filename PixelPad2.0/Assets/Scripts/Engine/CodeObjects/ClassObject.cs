﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClassObject : ContainerObject {

    public Dictionary<string, FunctionObject> defaultFuncs
    {
        get
        {
            if (_defaultFuncs == null)
            {
                SetupDefaultFuncs();
            }
            return _defaultFuncs;
        }
    }

    private Dictionary<string, FunctionObject> _defaultFuncs;

    public static int GetID ()
    {
        return sID++;
    }

    private static int sID;

    public int ID;

    public string name;
    private DefaultBehaviour d;

    void SetupDefaultFuncs ()
    {
        _defaultFuncs = new Dictionary<string, FunctionObject>();
        AddFunction(new FunctionObject()
        {
            name = "getkey",
            container = this,
            parameters = GetParams("keyName")
        });
    }

    List<ReferenceObject> GetParams (params string[] names)
    {
        var parameters = new List<ReferenceObject>();
        foreach(string n in names)
        {
            parameters.Add(new ReferenceObject() {
                name = n,
                container = this
            });
        }
        return parameters;
    }

    void AddFunction (FunctionObject functionObject)
    {
        defaultFuncs.Add(functionObject.name, functionObject);
    }

    //Used to create default variables such as x,y
    void AddVariable <T>(string varName, object varValue) where T : ValueObject, new()
    {
        VariableObject vo = new VariableObject();
        vo.container = this;
        vo.name = varName;
        T valO = new T();
        valO.SetValue(varValue);
        vo.SetValue(valO);
        variables.Add(varName, vo);
    }

    public void RegisterAllVars ()
    {
        AddVariable<IntObject>("x", 0);
        AddVariable<IntObject>("y", 0);
        AddVariable<StringObject>("sprite", "");

        foreach (ExecutableObject exec in executables)
        {
            AssignObject asObj = exec as AssignObject;
            if (asObj != null)
            {
                asObj.Execute();
            }
        }
    }

    public void ConnectFunctionParams ()
    {
        foreach (FunctionObject fo in functions.Values)
        {
            fo.ConnectParams();
        }
    }

    //Cancels original execution of all lines
    protected override void OnExecute()    {
        //Cross-reference
        d = new GameObject().AddComponent<DefaultBehaviour>();
        d.name = name;
        d.c = this;
        //Debug.LogFormat("Executing class {0}", name);
        Refresh();

        //Constructor execution?
    }

    public override BaseObject GetClone()
    {
        return base.GetClone();
    }

    public override string GetString()
    {
        return string.Format("{0} ({1})", base.GetString(), name);
    }

    public ValueObject CallFunction(FunctionObject fo)
    {
        List<object> values = new List<object>();
        foreach (ReferenceObject ro in fo.parameters)
        {
            values.Add(fo.parameters[0].GetVariable().GetValueObject().GetValue());
            //TODO Why cna't I directly get the value object? Have to go through Variable?
            //Debug.LogFormat("Calling {0} with parameter {1}", fo.name, ro.GetVariable().GetValueObject().GetValue());
        }

        switch (fo.name)
        {
            case "getkey":
                return new BoolObject()
                {
                    value = Input.GetKey(values[0].ToString())
                };
            default:
                Debug.LogWarningFormat("Unrecognized function {0}. Check if it's in the Switch-Case.", fo.name);
                return null;
        }
    }
}
