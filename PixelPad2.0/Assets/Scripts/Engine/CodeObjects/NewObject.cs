﻿using UnityEngine;

public class NewObject : ExecutableObject
{
    public string name;
    //We create a variable-object to store the class in.
    public ClassVarObject classVar;

    protected override void OnExecute()
    {
        classVar = new ClassVarObject();
        ClassObject newClass = (ClassObject)MainCompiler.instance.nameToClass[name].GetClone();
        classVar.SetValue(newClass);
        newClass.Execute();
    }

    public override ValueObject GetValueObject()
    {
        //If we haven't executed yet, this will create a new class.
        Execute();
        return classVar;
    }
}