﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// For when a variable is declared!
/// </summary>
public class VariableObject : BaseObject
{
    /// <summary>
    /// This is used to dereference variables after they ran in their local scope.
    /// </summary>
    public bool markedForDeletion;

    public string name;

    private ValueObject obj;

    public override ValueObject GetValueObject()
    {
        return obj;
    }

    public void SetValue(ValueObject v)
    {
        obj = v;
    }

    public void SetValueFromVariable(ValueObject v)
    {
        SetValue(v);
    }

    public override BaseObject GetClone()
    {
        //The ValueObject should be copied
        VariableObject clone = (VariableObject)base.GetClone();
        clone.obj = (ValueObject)obj.GetClone();
        return clone;
    }

}
