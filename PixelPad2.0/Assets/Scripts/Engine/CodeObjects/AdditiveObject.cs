﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdditiveObject : ModifyObject {

    protected override int priority
    {
        get
        {
            return 10;
        }
    }

    public override void Modify()
    {
        result = GetAdjacentValueObject(0).Add(GetAdjacentValueObject(1));
        //Debug.LogFormat("Additive result: {0} + {1} = {2}", values[0].GetValue() , values[1].GetValue(), result.GetValue());
    }

}
