﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrayObject : ValueObject
{
    public List<ReferenceObject> list = new List<ReferenceObject>();

    public override object GetValue()
    {
        return list;
    }

    public override void SetValue(object v)
    {
        list = v as List<ReferenceObject>;
    }

    public override ReferenceObject GetIndex(IntObject i)
    {
        return list[i.value];
    }
}
