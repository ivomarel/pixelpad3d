﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssignObject : ExecutableObject
{
    protected override void OnExecute() {
        
        //We get whatever object is in front of the AssignObject
        ReferenceObject toBeAssigned = line.objs[index - 1] as ReferenceObject;

        //We get whatever object is after the AssignObject
        BaseObject valueObject = line.objs[index + 1] as BaseObject;

        //Debug.LogFormat("Assigning {0} to {1}", toBeAssigned.GetVariable().name, valueObject.GetValueObject().GetValue());

        if (toBeAssigned != null)
            toBeAssigned.GetVariable().SetValueFromVariable(valueObject.GetValueObject());
        else
            Debug.LogWarningFormat("Error message at {0} ", line.ToString());
    }
}
