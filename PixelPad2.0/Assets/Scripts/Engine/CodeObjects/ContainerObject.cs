﻿
using UnityEngine;
using System.Collections.Generic;

public class ContainerObject : ExecutableObject
{
    //These lists are created to have a faster way of accessing variables or executables
    //The same info could be given by looking through 'lines'
    public List<ExecutableObject> executables = new List<ExecutableObject>();
    protected Dictionary<string, VariableObject> variables = new Dictionary<string, VariableObject>();
    public Dictionary<string, FunctionObject> functions = new Dictionary<string, FunctionObject>();

    //All lines of code - not sure if we'll need this
    public List<Line> lines = new List<Line>();

    //E.g. when there's a return statement in the function
    public bool executionHalted;

    protected override void OnExecute()
    {
        foreach (ExecutableObject obj in executables)
        {
            if (executionHalted)
                break;
            obj.Execute();
        }

        foreach (ExecutableObject obj in executables)
        {
            obj.Refresh();
        }

        Refresh();

        //Classes don't clear their variables but other containers do
        if (GetType() != typeof(ClassObject)) {
            ClearVars();
        }
    }

    void ClearVars ()
    {
        foreach(var vo in variables)
        {
            vo.Value.markedForDeletion = true;
        }
        variables.Clear();
    }

    public void SetVar(VariableObject v)
    {
        variables[v.name] = v;
    }

    //Not used?
    public void AddVar(VariableObject obj)
    {
        variables.Add(obj.name, obj);
    }

    public bool HasVar(string varName)
    {
        return (variables.ContainsKey(varName));
    }

    public VariableObject GetVar(string varName)
    {
        if (variables.ContainsKey(varName))
            return variables[varName];

        ContainerObject curr = container;
        //If this container does not have the variable, perhaps a parent container (func/class has it)
        while (curr != null)
        {
            if (curr.HasVar(varName))
            {
                return curr.GetVar(varName);
            }
            curr = curr.container;
        }

        //Variables are not really like the other objects now. They are not compiled anymore.
        variables.Add(varName, new VariableObject()
        {
            name = varName,
            container = this
        });

        return variables[varName];
        /*
        
        return null;
        */
    }

    //Creating a Clone means creating copies of all lists, but because lists would reference all original objects, we need to refill them
    public override BaseObject GetClone()
    {
        ContainerObject containerClone = (ContainerObject)base.GetClone();
        containerClone.lines = new List<Line>();
        containerClone.executables = new List<ExecutableObject>();
        containerClone.variables = new Dictionary<string, VariableObject>();
        containerClone.functions = new Dictionary<string, FunctionObject>();
        
        //TODO the lineString and wordString references are not copied. Maybe this is fine because we can leave the original reference
        /*
        foreach (Line lineOriginal in lines)
        {
            Debug.Log(lineOriginal.ToString());
        }
        */
        foreach (Line lineOriginal in lines)
        {
            Line lineClone = lineOriginal.GetClone();
            containerClone.lines.Add(lineClone);

            //Manually copy executables and variables
            foreach (BaseObject objClone in lineClone.objs)
            {
                objClone.container = containerClone;
                ExecutableObject e = objClone as ExecutableObject;
                if (e != null)
                {
                    containerClone.executables.Add(e);

                    FunctionObject f = objClone as FunctionObject;
                    if (f != null)
                    {
                        containerClone.functions.Add(f.name, f);
                    }
                }
            }
        }

        foreach (var v in variables)
        {
            containerClone.variables.Add(v.Key, (VariableObject)v.Value.GetClone());
        }

        //This might not be necessary but just making sure
        containerClone.Refresh();

        return containerClone;
    }

    public ClassObject GetClass()
    {
        ContainerObject curr = this;
        while (curr != null)
        {
            ClassObject co = curr as ClassObject;
            if (co != null)
            {
                return co;
            }
            curr = curr.container;
        }
        return null;
    }

    public override void Refresh()
    {
        executionHalted = false;
        base.Refresh();
    }
}

public class Line
{
    //lineNr is stored for allowing the console to go back to this line when there's an error.
    public int nr;
    //Not sure what index is used for... I think it can be removed
    public int index;
    public List<BaseObject> objs = new List<BaseObject>();

    /// <summary>
    /// This is the main clone system
    /// </summary>
    /// <returns></returns>
    public Line GetClone()
    {
        Line l = (Line)MemberwiseClone();
        l.objs = new List<BaseObject>();
        foreach (BaseObject bo in objs)
        {
            BaseObject clone = bo.GetClone();
            clone.line = l;
            l.objs.Add(clone);
        }
        return l;
    }

    public override string ToString()
    {
        System.Text.StringBuilder stringBuilder = new System.Text.StringBuilder();
        foreach (BaseObject obj in objs)
        {
            stringBuilder.Append(obj.GetType());
            stringBuilder.Append(": ");
            stringBuilder.Append(obj.GetOriginalWord());
            stringBuilder.Append(" || ");
        }
        return stringBuilder.ToString();
    }
}
