﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Used to store objects of a class
public class ClassVarObject : ValueObject
{
    //We use this so we cann not say somthing like Vector3 p = new GameObject();
    //Both would be ClassVars but different types

    public string classType;  
    public ClassObject value; 

    public override object GetValue()
    {
        return value;
    }

    public override void SetValue(object v)
    {
        value = (ClassObject)v;
    }

    public override string GetString()
    {
        return string.Format("{0} ({1})", base.GetString(), classType) ;
    }
}
