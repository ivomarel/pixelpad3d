﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClassCompiler : BaseCompiler
{
    public override BaseObject Compile(ContainerObject cont, LineString line, ref int i)
    {
        ClassObject c = CreateCodeObject<ClassObject>(cont, line);
        c.name = line.words[++i].word;
        // Debug.LogFormat("Compiled class {0}", c.name);
        //TEMP FOR TESTING DIFFERENT CLASSOBJECTS
        c.ID = ClassObject.GetID();
        MainCompiler.instance.currentClass = c;
        MainCompiler.instance.nameToClass.Add(c.name, c);
        
        return c;
    }

    public override bool IsType(string name)
    {
        return name == "class";
    }

}
