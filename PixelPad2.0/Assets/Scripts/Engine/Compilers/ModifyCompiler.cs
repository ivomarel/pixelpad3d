﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModifyCompiler : BaseCompiler
{
    public override BaseObject Compile(ContainerObject cont, LineString line, ref int i)
    {
        string word = line.words[i].word;

        ModifyObject mo = null;
        switch (word)
        {
            case "*":
                mo = CreateCodeObject<MultiplyObject>(cont, line);
                break;
            case "+":
                mo = CreateCodeObject<AdditiveObject>(cont, line);
                break;
            case "-":
                mo = CreateCodeObject<DeductObject>(cont, line);
                break;
            default:
                Debug.LogWarningFormat("Uknown modifier word {0} . Please check.", word);
                break;
        }
        return mo;
    }

    //Instead of allowing multiple types, perhaps I can extend this by 4 different compilers such as AdditiveCompiler?
    public override bool IsType(string name)
    {
        return name == "+" || name == "-" || name == "*" || name =="/";
    }
}
