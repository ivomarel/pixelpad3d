﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrayCompiler : BaseCompiler
{
    public override BaseObject Compile(ContainerObject cont, LineString line, ref int i)
    {
        WordString ws = line.words[i];
        if (ws.word == "]")
            return null;

        Line currentLine = MainCompiler.instance.currentLine;
        BaseObject currentObject = currentLine.objs[currentLine.objs.Count-1];
        ReferenceObject refObj = currentObject as ReferenceObject;
        if (refObj != null)
        {
            //This is an index from a certain object
            IndexObject io = CreateCodeObject<IndexObject>(cont, line);
            return io;
        }

        //If it's not an index, it's just a regular array
        ArrayObject arrayObject = CreateCodeObject<ArrayObject>(cont, line);
        for (int j = i; j < line.words.Length; j++)
        {
            WordString nws = line.words[j];
            if (nws.word == "]")
            {
                //End
                i = j;
                break;
               
            } else if (nws.word == ",")
            {
                //Continue
            }
            else
            {
                //Compile the separate words
                ReferenceCompiler rc = new ReferenceCompiler();
                if (rc.IsType(nws.word))
                {
                    ReferenceObject obj = rc.Compile(cont, line, ref j) as ReferenceObject;
                    arrayObject.list.Add(obj);
                }
            }
        }
        
        return arrayObject;
    }

    public override bool IsType(string name)
    {
        return name == "[" || name == "]";
    }
}
