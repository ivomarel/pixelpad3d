﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseCompiler {

    public abstract bool IsType(string name);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="words"></param>
    /// <param name="index"></param>
    /// <returns>The next index. Usually the same but some objects will skip the word after it</returns>
    public abstract BaseObject Compile(ContainerObject cont, LineString line, ref int i);

    //Got heavily simplified now that we do all this in our MainCompiler
    protected T CreateCodeObject<T>(ContainerObject container, LineString lineString) where T : BaseObject, new()
    {
        T codeObject = new T();
        //Assign container. This is used in execution and compilation
        codeObject.container = container;

        //Adding this shortcut like this, can't do it in ExecutableObject since that's an abstract class
        ExecutableObject exec = codeObject as ExecutableObject;
        if (exec != null)
        {
            container.executables.Add(exec); 
        }

        codeObject.line = MainCompiler.instance.currentLine;
        codeObject.index = MainCompiler.instance.currentLine.objs.Count;

        MainCompiler.instance.currentLine.objs.Add(codeObject);

        return codeObject;
    }

    /*
    protected ArrayObject<T> CreateArrayObject<T>(ContainerObject container, LineString lineString) where T : VariableObject{
        ArrayObject<T> arrayObject = new ArrayObject<T>();

        return arrayObject;
    }
    */
}
