﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssignCompiler : BaseCompiler
{
    public override BaseObject Compile(ContainerObject cont, LineString line, ref int i)
    {
        AssignObject o = CreateCodeObject<AssignObject>(cont, line);

        return o;
    }

    public override bool IsType(string name)
    {
        return name == "=";
    }
}
