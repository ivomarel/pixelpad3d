﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnCompiler : BaseCompiler
{
    public override BaseObject Compile(ContainerObject cont, LineString line, ref int i)
    {
        ReturnObject po = CreateCodeObject<ReturnObject>(cont, line);

        return po;
    }

    public override bool IsType(string name)
    {
        return name == "return";
    }
}
