﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ClassVarCompiler : BaseCompiler
{
    public override BaseObject Compile(ContainerObject cont, LineString line, ref int i)
    {
        NewObject obj = CreateCodeObject<NewObject>(cont, line);
        obj.name = line.words[i].word;
        return obj;
    }

    public override bool IsType(string name)
    {
        return MainCompiler.instance.HasClass(name);
    }
}

