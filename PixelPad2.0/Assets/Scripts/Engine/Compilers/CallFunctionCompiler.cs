﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CallFunctionCompiler : BaseCompiler
{
    private ClassObject currentClass
    {
        get
        {
            return MainCompiler.instance.currentContainer.GetClass();
        }
    }

    public override BaseObject Compile(ContainerObject cont, LineString line, ref int i)
    {
        WordString wordString = line.words[i];

        CallFunctionObject obj = CreateCodeObject<CallFunctionObject>(cont, line);

        //TODO Should the defaultFuncs not be static to make them object dependent? Not sure.
        if (currentClass.defaultFuncs.ContainsKey(wordString.word))
        {
            obj.functionRef = currentClass.defaultFuncs[wordString.word];
        }
        else
        {
            obj.functionRef = currentClass.functions[wordString.word];
        }
        return obj;
    }

    //TODO come up with way to detect if this is a class.
    public override bool IsType(string name)
    {
        return currentClass.functions.ContainsKey(name) || currentClass.defaultFuncs.ContainsKey(name);
    }
}
