﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompareCompiler : BaseCompiler
{
    public override BaseObject Compile(ContainerObject cont, LineString line, ref int i)
    {
        string word = line.words[i].word;

        CompareObject co = CreateCodeObject<CompareObject>(cont, line); ;
        switch (word)
        {
            case "==":
                co.type = CompareType.Equal;
                break;
            case "!=":
                co.type = CompareType.NotEqual;
                break;
            case ">":
                co.type = CompareType.Greater;
                break;
            case "<":
                co.type = CompareType.Less;
                break;
            default:
                Debug.LogWarningFormat("Uknown modifier word {0} . Please check.", word);
                break;
        }
        return co;
    }

    //Instead of allowing multiple types, perhaps I can extend this by 4 different compilers such as AdditiveCompiler?
    public override bool IsType(string name)
    {
        return name == "==" || name == "!=" || name == ">" || name == "<";
    }
}
