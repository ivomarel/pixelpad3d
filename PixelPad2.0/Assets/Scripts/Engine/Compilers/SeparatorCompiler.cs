﻿
class SeparatorCompiler : BaseCompiler

{
    public override BaseObject Compile(ContainerObject cont, LineString line, ref int i)
    {
        SeparatorObject separatorObject = CreateCodeObject<SeparatorObject>(cont, line);
        return separatorObject;
    }

    public override bool IsType(string name)
    {
        return name == ",";
    }
}
