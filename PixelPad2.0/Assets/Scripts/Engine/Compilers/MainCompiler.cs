﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class MainCompiler : Singleton<MainCompiler>
{
    bool logDetails = true;


    private static List<BaseCompiler> compilers = new List<BaseCompiler>()
    {
        new ClassCompiler(),
        new ArrayCompiler(),
        new AssignCompiler(),
        new PrintCompiler(),
        new FunctionCompiler(),
        new ModifyCompiler(),
        new ConditionCompiler(),
        //ClassVar is the new 'NewCompiler'
        new GroupCompiler(),
        new SeparatorCompiler(),
        new ClassVarCompiler(),
        new CallFunctionCompiler(),
        new ReturnCompiler(),
        new CompareCompiler(),
        //new IgnoreCompiler(),
        //HAS TO BE AT THE END
        new ReferenceCompiler()
    };

    public ClassObject mainClass
    {
        get
        {
            if (_mainClass == null)
                _mainClass = nameToClass["Main"];
            return _mainClass;
        }
    }
    private ClassObject _mainClass;


    //Used by other classes
    public ClassObject currentClass;
    public ContainerObject currentContainer;
    public Line currentLine;

    //The namespace is just a container for the classes
    private ClassObject nameSpace;
    private string currentClassString;

    private ProjectData projectData;

    public List<ClassString> classSplitStrings;
    public Dictionary<string, ClassObject> nameToClass;

    public void Compile(ProjectData data)
    {
        projectData = data;

        var watch = System.Diagnostics.Stopwatch.StartNew();
        
        nameSpace = new ClassObject();
        nameSpace.name = "PixelPad";

        classSplitStrings = new List<ClassString>();
        nameToClass = new Dictionary<string, ClassObject>();
        //We separate all big strings into lines and words
        foreach (ClassData c in projectData.classes)
        {
            classSplitStrings.Add(CreateClassString(c.name, c.script));
        }

        //Compile classes, vars and funcs
        foreach (ClassString cs in classSplitStrings)
        {
            CompileClass(cs, 0);
        }

        //Register the vars to the class
        foreach (ClassObject co in nameToClass.Values)
        {
            co.RegisterAllVars();
            co.ConnectFunctionParams();
        }

        //Compile everything inside
        foreach (ClassString cs in classSplitStrings)
        {
            CompileClass(cs, 1);
        }

        // the code that you want to measure comes here
        watch.Stop();
        Debug.LogFormat("Compilation complete in {0} ms!", watch.ElapsedMilliseconds);
    }

    public void Play ()
    {
        mainClass.Execute();
    }

    #region StringToLines

    int lineNr;

    ClassString CreateClassString(string className, string data)
    {
        ClassString cs = new ClassString();
        //Split into multiple lines (can be optimized)
        string[] linesRaw = data.Split(("\n").ToCharArray());
        List<string> lines = new List<string>();

        //First line is a class-line. E.g. class Block:
        //Before this had to be written at the start of each script but now we do it automatically
        lines.Add("class " + className + ":");

        foreach (string line in linesRaw)
        {
            lines.Add(line);
        }

        cs.lines = new LineString[lines.Count];

        for (int i = 0; i < lines.Count; i++)
        {
            //Storing lineNr for other functions
            lineNr = i;

            string line = lines[i];

            //Remove empty lines
            if (line.Trim().Length == 0)
                continue;

            int currentTabs = CountTabs(line); 

            if (i != 0) currentTabs++;// We add one because only class is at 0


            SplitWords(line);

            LineString ls = new LineString();
            ls.lineNr = lineNr;
            ls.nTabs = currentTabs;
            ls.words = new WordString[splitWordList.Count];

            for (int j = 0; j < splitWordList.Count; j++)
            {
                WordString ws = new WordString();
                ws.word = splitWordList[j];
                ls.words[j] = ws;
            }

            cs.lines[i] = ls;
        }
        return cs;
    }

    int CountTabs(string line)
    {
        print(line);
        int i = 0;

        int nSpaces = 0;
        int nTabs = 0;

        while (i < line.Length)
        {
            if (line[i] == ' ')
            {
                nSpaces++;
                if (nSpaces == 4) {
                    nSpaces = 0;
                    nTabs++;
                }
            } else if (char.IsWhiteSpace(line[i])) {
                
                nSpaces = 0;
                nTabs++;
            } else {
                Debug.LogFormat("This line has {0} symbol", line[0]);

                break;
            }
            i++;
        }
        Debug.LogFormat("This line has {0} tabs", nTabs);
        return nTabs;
    }

    List<string> splitWordList;
    StringBuilder sb;
    char firstChar;

    void SplitWords(string line)
    {
        splitWordList = new List<string>();

        sb = new StringBuilder();

        for (int i = 0; i < line.Length; i++)
        {
            char c = line[i];

            // Before anything else, String check
            if (sb.Length > 0 && firstChar == '"')
            {
                sb.Append(c);
                if (c == '"')
                {
                    Debug.Log("Compiled string " + sb.ToString());
                    EndWord();

                }
                continue;
            }

            //Comment
            if (c == '#')
            {
                return;
            }

            if (c == ' ' || char.IsWhiteSpace(c))
            {
                EndWord();
                continue;
            }

            //If it's the first character, always add it.
            if (sb.Length == 0)
            {
                firstChar = c;
                sb.Append(c);
                //Parentheses are always alone.
                if (c == '(' || c == ')' || c == '[' || c == ']' || c == ',')
                    EndWord();
                continue;
            }


            //Variablenames can have letters of numbers
            if (char.IsLetter(firstChar))
            {
                if (char.IsLetter(c) || char.IsDigit(c))
                {
                    sb.Append(c);
                }
                //For now we just continue after a .
                else if (c == '.')
                {
                    sb.Append(c);
                }
                else
                {
                    EndWord();
                    i--;
                }
                continue;
            }

            //Numbers can be longer than one character
            if (char.IsDigit(firstChar))
            {
                if (char.IsDigit(c))
                {
                    sb.Append(c);
                }
                else
                {
                    EndWord();
                    i--;
                }
                continue;
            }

            //Symbol combos such as &&, ||, != etc
            
            if (char.IsSymbol(firstChar) || IsModifySymbol(firstChar))
            {
                if (char.IsSymbol(c))
                {
                    sb.Append(c);
                }
                else
                {
                    Debug.LogFormat("The symbol {0} ends becaus {1} is not a compatible symbol", sb, c);
                    EndWord();
                    i--;
                }
                continue;
            }

            Debug.LogWarningFormat("Unrecognized symbol {0} in {1} on line {2}", c, currentClass, lineNr);
        }
        EndWord();
    }

    bool IsModifySymbol (char c)
    {
        return c == '-' || c == '*' || c == '+' || c == '/';
    }

    void EndWord()
    {
        //Add a word but ignore multiple spaces
        if (sb.Length > 0)
            splitWordList.Add(sb.ToString());
        sb = new StringBuilder();
        firstChar = ' ';
    }

    #endregion

    void CompileClass(ClassString cs, int iteration)
    {
        //Before each class we update the current container to the namespace again
        currentContainer = nameSpace;

        foreach (LineString ls in cs.lines)
        {
            //Empty/commented line
            if (ls == null || ls.words.Length == 0)
                continue;

            //The first iteration we ONLY look at the class info
            if (iteration == 0)
            {
                if (ls.nTabs > 1)
                {
                    continue;
                }
            }
            
            CreateObjectsForLine(ls);
        }
    }

    void CreateObjectsForLine(LineString ls)
    {
        ContainerObject nextContainer = null;
        currentLine = new Line();
        currentLine.nr = ls.lineNr;

        for (int i = 0; i < ls.words.Length; i++)
        {
            WordString ws = ls.words[i];

            //If it has been compiled before, skip it.
            if (ws.compiledObject != null)
            {
                //Debug.LogWarningFormat("Ignoring {0}, because it was already compiled", ws.word);
                //But in case this is a function, we should set the currentContainer
                ContainerObject co = ws.compiledObject as ContainerObject;
                if (co != null)
                {
                    currentContainer = co;
                }
                //We can skip the entire line since we compile everything as a whole line
                return;
            }

            //If it was tabbed back, we should switch back to the previous container
            while (currentContainer.lineString != null && ls.nTabs <= currentContainer.lineString.nTabs)
            {
                //Debug.LogFormat("Switching tab-back from {0} to {1}", currentContainer.GetString(), currentContainer.container.GetString());
                currentContainer = currentContainer.container;
            }

            foreach (BaseCompiler bc in compilers)
            {
                if (bc.IsType(ws.word))
                {
                    //Storing this to use it a few lines later
                    int stringIndex = i;
                    //Debug.LogFormat("Compiling {0}", word);
                    BaseObject bo = bc.Compile(currentContainer, ls, ref i);

                    //Ignore compiler
                    if (bo == null)
                    {
                        Debug.LogFormat("Ignoring {0}", ls.words[i].word);
                        break;
                    }  
                    
                    //TODO This should maybe be moved inside the Compiler function

                    //The wordstring is marked as 'compiled'
                    ws.compiledObject = bo;

                    //The BaseObject stores the original string for debugging purposes
                    bo.lineString = ls;
                    bo.stringIndex = stringIndex;

                    ContainerObject co = bo as ContainerObject;
                    if (co != null)
                    {
                        nextContainer = co;
                    }
                    //Break out of foreach.
                    break;
                }
            }
        }
        //We add our newly created line to the container
        currentLine.index = currentContainer.lines.Count;
        currentContainer.lines.Add(currentLine);

        if (logDetails)
        {
            Debug.Log(currentLine.ToString());
        }
        //We should only set currentContainer AFTER lines have been added to the previous container
        //This way a container is not it's own container
        if (nextContainer != null)
            currentContainer = nextContainer;
    }

    public bool HasClass (string name)
    {
        foreach(ClassData co in projectData.classes)
        {
            if (co.name == name)
                return true;
        }
        return false;
    }

}

public class ClassString
{
    public LineString[] lines;

    public override string ToString()
    {
        StringBuilder sb = new StringBuilder();
        foreach (LineString l in lines)
        {
            sb.Append(l.ToString());
            sb.Append('\n');
        }
        return sb.ToString();
    }
}

public class LineString
{
    public WordString[] words;
    public int nTabs;
    public int lineNr;

    public override string ToString()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append(nTabs);
        sb.Append(": ");
        foreach (WordString w in words)
        {

            sb.Append(w.ToString());
            sb.Append(": ");
            sb.Append(w.word);
            sb.Append(" | ");
        }

        return sb.ToString();
    }
}

public class WordString
{
    public string word;
    public BaseObject compiledObject;

    public override string ToString()
    {
        return word;
    }
}


public class CompilerList
{
    public CompilerList(List<BaseCompiler> _compilers)
    {
        compilers = _compilers;
    }

    public List<BaseCompiler> compilers;
}