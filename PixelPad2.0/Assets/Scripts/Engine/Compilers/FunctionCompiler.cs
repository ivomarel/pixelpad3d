﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FunctionCompiler : BaseCompiler
{
    public override BaseObject Compile(ContainerObject cont, LineString line, ref int i)
    {
        FunctionObject fo = CreateCodeObject<FunctionObject>(cont, line);
        fo.name = line.words[++i].word;
        
        fo.container.functions.Add(fo.name, fo);



        return fo;
    }

    public override bool IsType(string name)
    {
        return name == "def";
    }
}
