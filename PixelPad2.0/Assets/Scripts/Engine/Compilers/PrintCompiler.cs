﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrintCompiler : BaseCompiler
{
    public override BaseObject Compile(ContainerObject cont, LineString line, ref int i)
    {
        PrintObject po = CreateCodeObject<PrintObject>(cont, line);
        return po;
    }

    public override bool IsType(string name)
    {
        return name == "print";
    }
}
