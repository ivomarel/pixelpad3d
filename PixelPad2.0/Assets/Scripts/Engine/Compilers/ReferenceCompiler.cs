﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReferenceCompiler : BaseCompiler
{
    public override BaseObject Compile(ContainerObject cont, LineString line, ref int i)
    {
        string word = line.words[i].word;
        //Debug.LogFormat("Compiling {0} as reference obj", word);

        //The left-side of an = statement should be a variable
        ReferenceObject refVar = CreateCodeObject<ReferenceObject>(cont, line);
        refVar.name = word;
        //It can either be a string, int, bool, variable (unknown type)
        int x;
        if (int.TryParse(word, out x))
        {
            refVar.value = new IntObject()
            {
                value = x
            };
            return refVar;
        }

        bool b;
        if (bool.TryParse(word, out b))
        {
            refVar.value = new BoolObject()
            {
                value = b
            };
            return refVar;
        }

        //Strings
        if (word.StartsWith("\"") && word.EndsWith("\""))
        {
            refVar.value = new StringObject()
            {
                value = word.Trim('"')
            };
            return refVar;
        }

        
        //Create a regular variable and just set the word to later reference the original VariableObject

        return refVar;
    }

    public override bool IsType(string name)
    {
        //TEMP. Anything that's not anything else, is considered a variable
        return !(name == ":");
    }

    bool VariableExistsInOtherObject (string varPath, ContainerObject container)
    {
        ContainerObject curr = container;
        string[] parts = varPath.Split('.');
        //Some symbols can be ignored
        if (parts.Length <= 1)
            return false;

        for (int j = 0; j < parts.Length; j++)
        {
            string part = parts[j];
            if (!curr.HasVar(part))
            {
                Debug.LogWarningFormat("Trying to get value {0} from {1}, but this variable doesn't exist", part, curr.GetString());
                break;
            }

            //Value can be an actual value like and int or something, but could also be another container
            VariableObject var = curr.GetVar(part);

            if (j < parts.Length -1)
            {
                  //The ClassType should be used to see if the var exists in this ClassType in general
                ClassVarObject cv = var.GetValueObject() as ClassVarObject;
                if (cv != null)
                {
                    curr = MainCompiler.instance.nameToClass[cv.classType];
                }
            } else
            {
                if (var != null)
                {
                    return true;
                }
            }
        }
        Debug.LogErrorFormat("{0} was not recognized as a valid path to a variable.", varPath);
        return false;
    }

    
}
