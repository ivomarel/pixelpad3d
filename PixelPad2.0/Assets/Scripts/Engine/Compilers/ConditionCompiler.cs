﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConditionCompiler : BaseCompiler
{
    public override BaseObject Compile(ContainerObject cont, LineString line, ref int i)
    {
        ConditionObject co = CreateCodeObject<ConditionObject>(cont, line);
        
        return co;
    }

    public override bool IsType(string name)
    {
        return (name == "if");
    }
}
