﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroupCompiler : BaseCompiler
{

    public override BaseObject Compile(ContainerObject cont, LineString line, ref int i)
    {
        //Should groupObjects be compiled like this?
        GroupObject groupObject = CreateCodeObject<GroupObject>(cont, line);
        
        string w = line.words[i].word;
        if (w == ")")
        {
            groupObject.isOpening = false;
            //Find the corresponding bracket
            for (int j = MainCompiler.instance.currentLine.objs.Count-1; j >= 0; j--) 
            {
                GroupObject correspondingObj = MainCompiler.instance.currentLine.objs[j] as GroupObject;
                if (correspondingObj != null && correspondingObj.connectedObj == null && correspondingObj.isOpening) {
                    //Two-way reference
                    correspondingObj.connectedObj = groupObject;
                    groupObject.connectedObj = correspondingObj;
                    return groupObject;
                }
            }
            Debug.LogWarningFormat("Can't connect ) to an opening bracket on line {0}", line.ToString());
        } else
        {
            groupObject.isOpening = true;
        }
        return groupObject;

    }

    public override bool IsType(string name)
    {
        return name == "(" || name == ")";
    }
}
