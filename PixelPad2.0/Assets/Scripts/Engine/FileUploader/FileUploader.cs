﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FrostweepGames.Plugins.WebGLFileBrowser;

public class FileUploader : MonoBehaviour
{

    public AssetObject assetObjectOriginal;
    public RawImage contentRawImage;

    public Text fileNameText,
                fileInfoText;

    private void Awake()
    {
        assetObjectOriginal.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
#if UNITY_EDITOR
        EditorFilePicker.FileWasOpenedEvent += FileWasOpenedEventHandler;
#else
        FileBrowserDialogLib.FileWasOpenedEvent += FileWasOpenedEventHandler;
#endif
    }

    private void OnDisable()
    {
#if UNITY_EDITOR
        EditorFilePicker.FileWasOpenedEvent -= FileWasOpenedEventHandler;
#else
        FileBrowserDialogLib.FileWasOpenedEvent -= FileWasOpenedEventHandler;
#endif
    }

    public void OpenFileDialogButtonOnClickHandler()
    {
#if UNITY_EDITOR
        EditorFilePicker.LoadImage();
#else
        FileBrowserDialogLib.OpenFileDialog();
#endif
    }

    private void FileWasOpenedEventHandler(byte[] data, string fileName, string resolution)
    {
        Texture2D t2D = FileBrowserDialogLib.GetTexture2D(data, fileName);
        contentRawImage.texture = t2D;

        AssetObject assetObjectClone = Instantiate(assetObjectOriginal, assetObjectOriginal.transform.parent);
        assetObjectClone.gameObject.SetActive(true);
        assetObjectClone.Init(fileName, resolution);

        Vector2 size = new Vector2(t2D.width, t2D.height);
        Sprite sprite = Sprite.Create(t2D, new Rect(Vector2.zero, size), Vector2.one*.5f);
        AssetManager.instance.sprites.Add(fileName, sprite);
    }
}
