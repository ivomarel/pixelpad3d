﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

#if UNITY_EDITOR
using UnityEditor;

public static class EditorFilePicker {

    public static event Action<byte[], string, string> FileWasOpenedEvent;

    public static void LoadImage()
    {
        string path = EditorUtility.OpenFilePanel("Overwrite with png", "", "png,jpg,jpeg");
        if (path.Length != 0)
        {
            string fileName = Path.GetFileName(path);

            byte[] fileContent = File.ReadAllBytes(path);
            if (FileWasOpenedEvent != null)
                FileWasOpenedEvent(fileContent, fileName, "");
        }
    }
}
#endif
