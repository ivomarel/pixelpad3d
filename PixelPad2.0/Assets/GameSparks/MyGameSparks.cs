#pragma warning disable 612,618
#pragma warning disable 0114
#pragma warning disable 0108

using System;
using System.Collections.Generic;
using GameSparks.Core;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!

namespace GameSparks.Api.Requests{
		public class LogEventRequest_DeleteProject : GSTypedRequest<LogEventRequest_DeleteProject, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_DeleteProject() : base("LogEventRequest"){
			request.AddString("eventKey", "DeleteProject");
		}
		
		public LogEventRequest_DeleteProject Set_Name( string value )
		{
			request.AddString("Name", value);
			return this;
		}
	}
	
	public class LogChallengeEventRequest_DeleteProject : GSTypedRequest<LogChallengeEventRequest_DeleteProject, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_DeleteProject() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "DeleteProject");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_DeleteProject SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_DeleteProject Set_Name( string value )
		{
			request.AddString("Name", value);
			return this;
		}
	}
	
	public class LogEventRequest_LoadProjects : GSTypedRequest<LogEventRequest_LoadProjects, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_LoadProjects() : base("LogEventRequest"){
			request.AddString("eventKey", "LoadProjects");
		}
	}
	
	public class LogChallengeEventRequest_LoadProjects : GSTypedRequest<LogChallengeEventRequest_LoadProjects, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_LoadProjects() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "LoadProjects");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_LoadProjects SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
	}
	
	public class LogEventRequest_SaveProject : GSTypedRequest<LogEventRequest_SaveProject, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_SaveProject() : base("LogEventRequest"){
			request.AddString("eventKey", "SaveProject");
		}
		
		public LogEventRequest_SaveProject Set_Name( string value )
		{
			request.AddString("Name", value);
			return this;
		}
		
		public LogEventRequest_SaveProject Set_Data( string value )
		{
			request.AddString("Data", value);
			return this;
		}
	}
	
	public class LogChallengeEventRequest_SaveProject : GSTypedRequest<LogChallengeEventRequest_SaveProject, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_SaveProject() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "SaveProject");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_SaveProject SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_SaveProject Set_Name( string value )
		{
			request.AddString("Name", value);
			return this;
		}
		public LogChallengeEventRequest_SaveProject Set_Data( string value )
		{
			request.AddString("Data", value);
			return this;
		}
	}
	
}
	

namespace GameSparks.Api.Messages {


}
