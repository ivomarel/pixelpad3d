﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Text;

public class CodeEditor : MonoBehaviour
{

    [Header("References")]
    public TMP_InputField mainInput;
    public TextMeshProUGUI mainText;
    public TextMeshProUGUI linesText;
    public TextMeshProUGUI coloredText;
    public RectTransform highlight;
    public GameObject[] styleRefs;
    [Space]
    [Header("Editor Style")]
    public Color caretColor;
    public Color background;
    public Color lineHighlight;
    public Color lineCountBg;
    public Color lineCountTxt;
    public Color scrollbar;

    [Space]
    [Header("Syntax Style")]
    public Color keywordColor;
    public Color classColor;
    public Color typeColor;
    public Color commentColor;
    public Color stringColor;
    private int lineCount = 0;
    private bool workingParse = true;

    void Awake()
    {

        LoadTheme();
        mainInput.onValueChanged.AddListener(WriteEvent);
    }

    private void Start()
    {
        //Ensuring the text is updated when first opened
        WriteEvent(mainInput.text);
    }

    void Update()
    {

        if (Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.Backspace) || Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow) || Input.GetMouseButton(0))
        {
            HighlightLine();
        }

        //Input field paste bugfix
        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyUp(KeyCode.V))
        {
            workingParse = false;
        }

        if (Input.GetKeyUp(KeyCode.LeftControl))
        {
            workingParse = true;
            WriteEvent(mainInput.text);
        }
    }

    public void HighlightLine(string input = null)
    {

        highlight.anchoredPosition = new Vector2(5, mainText.textInfo.lineInfo[mainText.textInfo.characterInfo[0].lineNumber].lineHeight * (-mainText.textInfo.characterInfo[mainInput.caretPosition].lineNumber) - 4 + mainText.GetComponent<RectTransform>().anchoredPosition.y);

    }

    public void WriteEvent(string input)
    {

        coloredText.text = ParseOutput(input);

        if (mainInput.verticalScrollbar.size < 1)
        {
            mainInput.verticalScrollbar.gameObject.SetActive(true);
        }
        else
        {
            mainInput.verticalScrollbar.gameObject.SetActive(false);
        }

        int actualCount = input.Split('\n').Length;

        if (actualCount != lineCount)
        {
            lineCount = actualCount;
            string lineString = "";
            for (int i = 1; i < actualCount + 1; i++)
            {
                lineString += i + "\n";
            }
            linesText.text = lineString;
        }

        HighlightLine();

    }

    string ParseOutput(string input)
    {
        if (!workingParse) { return input; }

        int delta = 0;
        string color = null;

        // string comments = @"(\/\/.+?$|\/\*.+?\*\/)";   
        //  MatchCollection commentMatches = Regex.Matches(input, comments, RegexOptions.Multiline);
        string comments = @"\#.+?$";
        MatchCollection commentMatches = Regex.Matches(input, comments, RegexOptions.Multiline);
        delta = 0;
        color = ColorUtility.ToHtmlStringRGB(commentColor);

        foreach (Match m in commentMatches)
        {
            input = input.Insert(m.Index + delta, "<#" + color + ">");
            delta += 9;
            input = input.Insert(m.Index + m.Length + delta, "</color>");
            delta += 8;
        }


        string keywords = @"\b(def|if)\b";
        MatchCollection keywordMatches = Regex.Matches(input, keywords);
        delta = 0;
        color = ColorUtility.ToHtmlStringRGB(keywordColor);

        foreach (Match m in keywordMatches)
        {
            input = input.Insert(m.Index + delta, "<#" + color + ">");
            delta += 9;
            input = input.Insert(m.Index + m.Length + delta, "</color>");
            delta += 8;
        }


        //TODO this should dynamically update
        var scriptToggles = ScreenManager.instance.GetScreenInstance<EngineScreen>().tabPanel.scriptToggles;
        StringBuilder sb = new StringBuilder();
        sb.Append(@"\b(");
        bool first = true;
        foreach (var toggle in scriptToggles)
        {
            if (!first)
                sb.Append("|");
            first = false;
            sb.Append(toggle.scriptName);
        }
        sb.Append(")\\b");
        string classes = sb.ToString();
      //  classes = @"\b(Main|PlayerShip|Bullet)\b";

        MatchCollection classesMatches = Regex.Matches(input, classes);
        delta = 0;
        color = ColorUtility.ToHtmlStringRGB(classColor);

        foreach (Match m in classesMatches)
        {
            input = input.Insert(m.Index + delta, "<#" + color + ">");
            delta += 9;
            input = input.Insert(m.Index + m.Length + delta, "</color>");
            delta += 8;
        }

        string types = @"[\-\+\*\=]";
        MatchCollection typeMatches = Regex.Matches(input, types);
        delta = 0;
        color = ColorUtility.ToHtmlStringRGB(typeColor);

        foreach (Match m in typeMatches)
        {
            input = input.Insert(m.Index + delta, "<#" + color + ">");
            delta += 9;
            input = input.Insert(m.Index + m.Length + delta, "</color>");
            delta += 8;
        }

        string strings = "\".+?\"";
        MatchCollection stringMatches = Regex.Matches(input, strings);
        delta = 0;
        color = ColorUtility.ToHtmlStringRGB(stringColor);

        foreach (Match m in stringMatches)
        {
            input = input.Insert(m.Index + delta, "<#" + color + ">");
            delta += 9;
            input = input.Insert(m.Index + m.Length + delta, "</color>");
            delta += 8;
        }

        return input;

    }

    public void LoadTheme()
    {
        mainInput.caretColor = caretColor;
        styleRefs[0].GetComponent<Image>().color = background;
        highlight.GetComponent<Image>().color = lineHighlight;
        styleRefs[1].GetComponent<Image>().color = lineCountBg;
        linesText.color = lineCountTxt;
        styleRefs[2].GetComponent<Image>().color = scrollbar;
    }

}