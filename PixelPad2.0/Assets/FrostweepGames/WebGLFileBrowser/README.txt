-INFO:
WEBGL NATIVE FILE BROWSER
CURRENT VERSION 1.0
POWERED BY FROSTWEEP GAMES
PROGRAMMER ARTEM SHYRIAIEV
LAST UPDATE JANUARY 10 2018

-PATCHLIST:
VERSION 1.0 - IMPLEMENTED WEBGL NATIVE FILE BROWSER


-WARNING
for correct working of the plugin you need to add this code in your index.html page where uses webgl native file browser:


<!-- BEGIN WEBGL FILE BROWSER LIB -->
<form id="fileBrowserPopup" style="display: none;">
		<img src="TemplateData/2x2.png" style="position: absolute; width: 100%; height: 100%;"/>
		<img src="TemplateData/White-Button.png" style="position: absolute;  top: 35%; left: 38%; width: 25%; height: 28%;"/>

        <label for="fileToUpload">
          <img src="TemplateData/upload_button.png" style="position: absolute; top: 45%; left: 42.8%; width: 16%; height: 10%;"/>
        </label>
        <input type="File" name="fileToUpload" id="fileToUpload" style="width: 0px; height: 0px;" onchange="sendfile(event);return false;" />
</form>
	
<script type='text/javascript'>
    function sendfile(e){
        var files = e.target.files;
        for (var i = 0, f; f = files[i]; i++) {
            var reader = new FileReader();
            reader.onload = (function(file) {
                return function(e) {
					gameInstance.SendMessage('FileBrowserDialogLib', 'ApplyFileName', escape(file.name));
				
                    (window.filedata = window.filedata ? window.filedata : {})[file.name] = e.target.result;
                    gameInstance.SendMessage("FileBrowserDialogLib", "FileUpload", file.name);
                }
            })(f);
            reader.readAsArrayBuffer(f);
        }
    }
</script>
<!-- END WEBGL FILE BROWSER LIB -->