﻿using UnityEngine;
using UnityEngine.UI;

namespace FrostweepGames.Plugins.WebGLFileBrowser.Examples
{
    public class Example : MonoBehaviour
    {
        public RawImage contentRawImage;

        public Button openFileDialogButton;

        public Text fileNameText,
                    fileInfoText;

        private void Start()
        {
            openFileDialogButton.onClick.AddListener(OpenFileDialogButtonOnClickHandler);
        }

        private void OpenFileDialogButtonOnClickHandler()
        {
            FileBrowserDialogLib.FileWasOpenedEvent += FileWasOpenedEventHandler;
            FileBrowserDialogLib.OpenFileDialog();
        }

        private void FileWasOpenedEventHandler(byte[] data, string name, string resolution)
        {
            if(resolution.Contains(".png") || resolution.Contains(".jpeg") || resolution.Contains(".jpg"))
                contentRawImage.texture = FileBrowserDialogLib.GetTexture2D(data, name);

            fileNameText.text = name;
            fileInfoText.text = "File Name: " + name + "\nFile Resolution: " + resolution;

            FileBrowserDialogLib.FileWasOpenedEvent -= FileWasOpenedEventHandler;
        }
    }
}