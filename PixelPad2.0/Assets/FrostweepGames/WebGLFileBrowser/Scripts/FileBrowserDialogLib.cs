﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

namespace FrostweepGames.Plugins.WebGLFileBrowser
{
    public class FileBrowserDialogLib : MonoBehaviour
    {
        /// <summary>
        /// byte[] - File Data
        /// string - File Name
        /// string - File Resolution
        /// </summary>
        public static event Action<byte[], string, string> FileWasOpenedEvent;

        private static string _FileName;
        private static bool _IsAlreadyActive;
        private static bool _ItWasInFullScreen;

        private static FileBrowserDialogLib _Instance;

        [DllImport("__Internal")]
        private static extern int GetFileDataLength(string name);

        [DllImport("__Internal")]
        private static extern void FreeFileData(string name);

        [DllImport("__Internal")]
        private static extern IntPtr GetFileData(string name);

        [DllImport("__Internal")]
        private static extern void OpenFilePopup();

        [DllImport("__Internal")]
        private static extern void HideFilePopup();

        private void Awake()
        {
            if (_Instance != null)
            {
                Destroy(gameObject);
                return;
            }
            _Instance = this;
            DontDestroyOnLoad(gameObject);
        }

        private void FileUpload(string file)
        {
            var length = GetFileDataLength(file);
            var ptr = GetFileData(file);
            var data = new byte[length];
            Marshal.Copy(ptr, data, 0, data.Length);

            HideFileDialog();
            FreeFileData(file);

            if (_ItWasInFullScreen)
                Screen.fullScreen = true;

            if (FileWasOpenedEvent != null)
                FileWasOpenedEvent(data, _FileName, GetFileResolution(_FileName));

            _IsAlreadyActive = false;
        }

        private void ApplyFileName(string name)
        {
            var split = name.Replace(@"\", "/").Split("/"[0]);
            _FileName = split[split.Length - 1];
        }

        /// <summary>
        /// Opens Native File Browser Dialog
        /// </summary>
        public static void OpenFileDialog()
        {
            if (_Instance == null)
                _Instance = new GameObject("FileBrowserDialogLib").AddComponent<FileBrowserDialogLib>();


            if (_IsAlreadyActive)
                return;

            if (Screen.fullScreen)
            {
                Screen.fullScreen = false;
                _ItWasInFullScreen = true;
            }
            else _ItWasInFullScreen = false;

            _IsAlreadyActive = true;

            OpenFilePopup();
        }

        /// <summary>
        /// Hides Native File Browser Dialog
        /// </summary>
        public static void HideFileDialog()
        {
            HideFilePopup();
        }

        public static string GetFileResolution(string name)
        {
            string[] split = name.Split('.');

            return "." + split[split.Length - 1];
        }

        public static Texture2D GetTexture2D(byte[] data, string name = "custom")
        {
            var texture = new Texture2D(2, 2, TextureFormat.ARGB32, false, true);

            texture.name = name;
            texture.LoadImage(data);
            texture.Apply();

            return texture;
        }

        public static Sprite GetSprite(byte[] data, string name = "custom")
        {
            var texture = GetTexture2D(data, name);

            var sprite = Sprite.Create(texture, 
                                        new Rect(Vector2.zero, new Vector2(texture.width, texture.height)), 
                                        Vector2.one / 2f, 
                                        100, 
                                        1, 
                                        SpriteMeshType.FullRect, 
                                        Vector4.zero);
            return sprite;
        }

        public static string GetBase64StringFromImage(Texture2D texture, bool isJPEG = true)
        {
            if (isJPEG)
                return Convert.ToBase64String(texture.EncodeToJPG());
            else
                return Convert.ToBase64String(texture.EncodeToPNG());
        }
    }
}