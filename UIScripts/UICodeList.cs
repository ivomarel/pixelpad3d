﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICodeList : Singleton<UICodeList> {



    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)) {
            UIClassObject c = GetComponentInChildren<UIClassObject>();
            PythonConvertor.Convert(c);
            /*
            Compiler.isRunning = !Compiler.isRunning;
            if (Compiler.isRunning) {
                ExecuteCode();
            }*/
        }
    }

    void ExecuteCode()
    {
        UIClassObject c = GetComponentInChildren<UIClassObject>();
        c.StoreValue();
       // c.LinkUp();
        Compiler.Run(c.obj);
    }

 
}
