﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UIBaseObjectGen<T> : UIBaseObject where T : BaseObject, new() {

    public T obj {
        get {
            return baseObj as T;
        }
        set {
            baseObj = value;
        }
    }

    public override void StoreValue () {
        obj = new T();
        base.StoreValue();
    }

    //Connecting variables as objects, rather than by string references. Needs to happen after variables were set in the container
    public override void LinkUp()
    {
        base.LinkUp();
    }
}
