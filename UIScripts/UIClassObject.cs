﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIClassObject : UIContainerObject<ClassObject> {

    public InputField classNameInput;

    public override void CreatePythonLine()
    {
        PythonConvertor.AddLine("class " + classNameInput.text + ":");
        base.CreatePythonLine();
    }
}
