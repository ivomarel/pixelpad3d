﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPrintObject : UIExecutableObject<PrintOject> {

    public InputField varInput;

    public override void StoreValue()
    {
        base.StoreValue();
   //     obj.valueObj = obj.container.GetVar(varInput.text);
    }

	public override void LinkUp()
    {
        base.LinkUp();

    }

    public override void CreatePythonLine()
    {
        PythonConvertor.AddLine("print(" + varInput.text + ")");
        base.CreatePythonLine();
    }
}
