﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIVariableObject<T> : UIBaseObjectGen<T> where T : VariableObject, new() {

    public InputField varNameInput;
    public Text varType;

}
