﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIConditionObject : UIContainerObject<ConditionObject> {

    public InputField variableInput;

    public override void StoreValue()
    {
        base.StoreValue();
        obj.keyName = variableInput.text;
    }

    public override void CreatePythonLine()
    {
        PythonConvertor.AddLine("if " + variableInput.text + ":");
        base.CreatePythonLine();
    }
}
