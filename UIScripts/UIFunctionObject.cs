﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIFunctionObject : UIContainerObject<FunctionObject> {

    public InputField functionNameInput;

    public override void StoreValue()
    {
        base.StoreValue();
        obj.name = functionNameInput.text;
    }

    public override void CreatePythonLine()
    {
        PythonConvertor.AddLine("def " + functionNameInput.text + "():");
        base.CreatePythonLine();
    }
}
