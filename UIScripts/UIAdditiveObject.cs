﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIAdditiveObject : UIExecutableObject<AdditiveObject> {

    public InputField[] intObjects;

	public override void StoreValue()
    {
        base.StoreValue();
    }

    public override void LinkUp()
    {
        base.LinkUp();
        obj.values = new IntObject[intObjects.Length];
        int i = 0;
        //foreach(InputField intObj in intObjects) {
        //    obj.values[i] = (IntObject)obj.container.GetVar(intObj.text);
        //    i++;
        //}
    }

    public override void CreatePythonLine()
    {
        PythonConvertor.AddLine(intObjects[0].text + " = " + intObjects[0].text + " + " + intObjects[1].text);
        base.CreatePythonLine();
    }
}
