﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIContainerObject<T> : UIExecutableObject<T> where T : ContainerObject, new(){


    public override void StoreValue()
    {
        base.StoreValue();

        foreach (Transform child in indent)
        {
            UIBaseObject uiObj = child.GetComponent<UIBaseObject>();
            uiObj.StoreValue();

            BaseObject internalObj = uiObj.baseObj;
            internalObj.container = obj;
            VariableObject varObj = internalObj as VariableObject;
            if (varObj != null)
            {
                obj.variables.Add(varObj);
                continue;
            }
            //TODO every line should execute?
            ExecutableObject exObj = internalObj as ExecutableObject;
            if (exObj != null)
            {
                obj.lines.Add(exObj);
                continue;
            }

            Debug.LogError("Something went wrong");
        }
    }

    public override void LinkUp()
    {
        base.LinkUp();
        foreach (Transform child in indent)
        {
            UIBaseObject uiObj = child.GetComponent<UIBaseObject>();
            uiObj.LinkUp();
        }
    }

    public override void CreatePythonLine()
    {
        PythonConvertor.Indent();
        foreach (Transform child in indent)
        {
            UIBaseObject uiObj = child.GetComponent<UIBaseObject>();
            uiObj.CreatePythonLine();
        }
        PythonConvertor.UnIndent();
        base.CreatePythonLine();
    }
}
