﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UIBaseObject : MonoBehaviour {


    public Transform indent
    {
        get
        {
            return transform.Find("Indent");
        }
    }


    internal BaseObject baseObj;

    public virtual void StoreValue()
    {
        
    }

    //Connecting variables as objects, rather than by string references. Needs to happen after variables were set in the container
    public virtual void LinkUp()
    {

    }

    public virtual void CreatePythonLine () {
       // PythonConvertor.AddLine("X");
    }
}
